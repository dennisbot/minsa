define([
    'AppRouter',
    'views/HomeView',
    'views/Admin/MiPerfilView',
    // 'mods/modSecurity',
    // 'config/confRutas',
    'views/Parametro/ParametroView',
    'views/PersonalSalud/PersonalSaludView',
    'views/TablaOcupacionEscala/TablaOcupacionEscalaView',
], function(
    AppRouter,
    HomeView,
    MiPerfilView,
    // modSecurity,
    // confRutas,
    ParametroView,
    PersonalSaludView,
    TablaOcupacionEscalaView
    ) {
    return {
        appRouter : null,
        container : '#container',
        // rutas : confRutas.rutas,
        initialize : function() {
            this.appRouter = new AppRouter;

            this.appRouter.route('escalas-remunerativas', 'showEscalasRemunerativas', this.showEscalasRemunerativas);
            this.appRouter.route('parametros', 'showParametros', this.showParametros);
            this.appRouter.route('personal-salud', 'showPersonalSalud', this.showPersonalSalud);

            this.appRouter.route('mi-perfil', 'showMiPerfil', this.showMiPerfil);
            this.appRouter.route('', 'homeAction', this.homeAction);

            Backbone.history.start();
        },
        showEscalasRemunerativas() {
            var tablaOcupacionEscalaView = new TablaOcupacionEscalaView();
            $(this.container).html(tablaOcupacionEscalaView.el);
            ViewManager.handle(tablaOcupacionEscalaView);
        },
        showParametros : function() {
            var parametroView = new ParametroView();
            $(this.container).html(parametroView.el);
            ViewManager.handle(parametroView);
        },
        showPersonalSalud : function() {
            var personalSaludView = new PersonalSaludView();
            $(this.container).html(personalSaludView.el);
            ViewManager.handle(personalSaludView);

        },
        showMiPerfil : function() {
            var miPerfilView = new MiPerfilView();
            $(this.container).html(miPerfilView.el);
            ViewManager.handle(miPerfilView);
            // this.selectItem('#mi-perfil');
        },
        homeAction : function(actions) {
            var homeView = new HomeView();
            $(this.container).html(homeView.el);
            ViewManager.handle(homeView);
            // We have no matching route, lets display the home page
            // this.selectItem('#' + actions);
            // this.appRouter.navigate('/', true);
        }
    };
});