define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var ParametroCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/parametros",
            // model: GerenciaModel,
            fetchCargoViewParams : function(options) {
              // console.log('options: ', options);
              if (options && options.idDependencia) {
                options.url = this.url + '/cargoViewParams/'
                                        + options.nivel
                                        + '/' + options.idDependencia;
              }
              // console.log('options.url: ', BASE_URL + options.url);
              return Backbone.Collection.prototype.fetch.call(this, options);
            },
            fetchPersonalSaludViewParams : function(options) {
              options.url = this.url + '/personalSaludViewParams/';
              // console.log('options.url: ', BASE_URL + options.url);
              return Backbone.Collection.prototype.fetch.call(this, options);
            },
        });
        // You don't usually return a collection instantiated
        return ParametroCollection;
})