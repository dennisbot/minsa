define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var PersonalSaludCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_PERSONAL_SALUD,
            url: "rest/personalsalud",
            // model: EmpleadoModel,
            fetchMinEmpleados : function(options) {
              options.url = this.url + '/toselect/';
              console.log('options.url: ', BASE_URL + options.url);
              return Backbone.Collection.prototype.fetch.call(this, options);
            },
        });
        // You don't usually return a collection instantiated
        return PersonalSaludCollection;
})