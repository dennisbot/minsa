define([
  'underscore',
  'backbone',
  // 'models/gerencia/GerenciaModel'
], function(
        _,
        Backbone
    ) {
        var ParametroCollection = Backbone.Collection.extend({
            url: "rest/parametro",
        });
        // You don't usually return a collection instantiated
        return ParametroCollection;
})