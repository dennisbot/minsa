define([
  'JSOG',
  'models/TablaOcupacionEscalaModel'
  ],
  function(
      jsog,
      TablaOcupacionEscalaModel
    ) {
    var TablaOcupacionEscalaCollection = Backbone.Collection.extend({
        url : "rest/tablaocupacionescala",
        model : TablaOcupacionEscalaModel,
        comparator : 'ordenPosicion',
        parse : function(response) {
          return jsog.decode(response);
        },
        fetchEscalasRemunerativasViewParams : function(options) {
          options.url = this.url + '/escalasRemunerativasViewParams';
          console.log('options.url: ', BASE_URL + options.url);
          return Backbone.Collection.prototype.fetch.call(this, options);
        },
        fetchEscalasRemunerativas : function(options) {
          options.url = this.url +
                        '/by/idperiodoescala/' +
                        options.idPeriodo +
                        '/idescala/' +
                        options.idEscala;
          console.log('options.url: ', BASE_URL + options.url);
          return Backbone.Collection.prototype.fetch.call(this, options);
        },

    })
    return TablaOcupacionEscalaCollection;
})