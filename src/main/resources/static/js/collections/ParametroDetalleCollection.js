define([
  'underscore',
  'backbone',
  'models/ParametroDetalleModel'
], function(
        _,
        Backbone,
        ParametroDetalleModel
    ) {
        var ParametroDetalleCollection = Backbone.Collection.extend({
            // url: PATH_SERVICE_GERENCIA,
            url: "rest/parametrodetalle",
            model: ParametroDetalleModel,
            comparator : 'ordenPosicion',
            // comparator : function(pdetalle) {
            //   return -pdetalle.get('ordenPosicion');
            // },

            fetchByIdParametro : function(options) {
              // console.log('options: ', options);
              if (options && options.idparametro) {
                options.url = this.url + '/parametro/'
                                        + options.idparametro;
              }
              // console.log('options.url: ', BASE_URL + options.url);
              return Backbone.Collection.prototype.fetch.call(this, options);
            },
        });
        // You don't usually return a collection instantiated
        return ParametroDetalleCollection;
})