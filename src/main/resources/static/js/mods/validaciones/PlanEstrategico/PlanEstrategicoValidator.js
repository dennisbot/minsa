define([], function() {

    var PlanEstrategicoValidator = function() {
        console.log('se ha creado una instancia de PlanEstrategicoValidator');
        this.propertyValidator = {
            'anio': 'el Campo "AÑO" es REQUERIDO',
            'nombrePlan': 'el Campo "NOMBRE DEL PLAN" es REQUERIDO',
        }
    }

    return PlanEstrategicoValidator;
});