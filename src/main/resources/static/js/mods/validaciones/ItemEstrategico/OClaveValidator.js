define([], function() {

    var OClaveValidator = function() {
        // console.log('se ha creado una instancia de OClaveValidator');
        this.propertyValidator = {
            'descripcionItem': 'el Campo "DESCRIPCIÓN DEL OBJETIVO CLAVE" es REQUERIDO',
        }
        this.customValidator = function(itemEstrategicoModel) {
            var responsables = itemEstrategicoModel.responsables;
            if (responsables.length == 0)
                return "necesita seleccionar al menos un responsable";
            return false;
        }
    }

    return OClaveValidator;
});