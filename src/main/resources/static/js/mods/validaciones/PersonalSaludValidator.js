define([], function() {

    var PersonalSaludValidator = function() {
        // console.log('se ha creado una instancia de PersonalSaludValidator');

        this.propertyValidator = {
            'nombres': 'El Campo "Nombres" es REQUERIDO',
            'paterno': 'El Campo "APaterno" es REQUERIDO',
            'usuario': 'El campo "Usuario" es REQUERIDO',
            'nroDocumento': 'El campo "Nro de Documento" es REQUERIDO',
            'parametroDetalleByIdTipoDocumento.idParametroDetalle': 'Debe elegir un tipo de documento para este usuario',
            'parametroDetalleByIdProfesion.idParametroDetalle': 'El campo "Profesión" es REQUERIDO',
        }
    }

    return PersonalSaludValidator;
});