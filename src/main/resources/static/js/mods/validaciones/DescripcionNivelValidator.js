define([
    ], function() {

    var DescripcionNivelValidator = function(update, url, id) {
        this.propertyValidator = {
            'idNivel': {
                'requerido' : {
                    'errorMessage' : 'el Campo "NIVEL" es REQUERIDO',
                },
                'isNumeric' : {
                    'errorMessage': 'el Campo "NIVEL" debe ser un NÚMERO ENTERO',
                },
            },
            'descripcionNivel': {
                'requerido' : {
                    'errorMessage' : 'el Campo "DESCRIPCIÓN DEL NIVEL" es REQUERIDO',
                }
            },
        }
        if (!update)
            this.propertyValidator.idNivel['exist'] =  {
                'errorMessage' : 'el "NIVEL" YA se encuentra REGISTRADO, eliga otro nivel',
                'url': url,
                'id': id,
            }
    }

    return DescripcionNivelValidator;
});