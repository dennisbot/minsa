define(
    [
        'mods/itemEstrategico/childs/OEstrategico',
        'mods/itemEstrategico/childs/OClave',
        'mods/itemEstrategico/childs/OEspecifico',
        'mods/itemEstrategico/childs/Iniciativa',
        'mods/itemEstrategico/childs/Meta',
    ],
    function(
        OEstrategico,
        OClave,
        OEspecifico,
        Iniciativa,
        Meta
) {
        return {
            getInstance : function(lvl) {
                switch(lvl) {
                    case 1:
                        if (this.OEstrategico)
                            return this.OEstrategico;
                        else
                            return this.OEstrategico = new OEstrategico();
                    case 2:
                        if (this.OClave)
                            return this.OClave;
                        else
                            return this.OClave = new OClave();
                    case 3:
                        if (this.OEspecifico)
                            return this.OEspecifico;
                        else
                            return this.OEspecifico = new OEspecifico();
                    case 4:
                        if (this.Iniciativa)
                            return this.Iniciativa;
                        else
                            return this.Iniciativa = new Iniciativa();
                    case 5:
                        if (this.Meta)
                            return this.Meta;
                        else
                            return this.Meta = new Meta();
                }
                return null;
            }
        }
})