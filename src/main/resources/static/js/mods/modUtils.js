define(['cookiejs'], function(cookiejs) {
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    }

    String.prototype.repeat = function( num )
    {
        return new Array( num + 1 ).join( this );
    }

    var toTitleCase = function(str) {
        return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }

    var processCustomMessages = function() {
        if ($.cookie('successMessage')) {
            $.notify($.cookie('successMessage'), "success");
            $.removeCookie('successMessage');
        }
        if ($.cookie('errorMessage')) {
            $.notify($.cookie('errorMessage'), "error");
            $.removeCookie('errorMessage');
        }
        if ($.cookie('warnMessage')) {
            $.notify($.cookie('warnMessage'), "warn");
            $.removeCookie('warnMessage');
        }
        if ($.cookie('infoMessage')) {
            $.notify($.cookie('infoMessage'), "info");
            $.removeCookie('infoMessage');
        }

    }
    var createCookie = function(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    var readCookie = function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length).replace(/"/g, '');
        }
        return null;
    }

    var getMonths = function() {
        var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
            "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"];
        return monthNames;
    }

    var getTwoYearAbove = function() {
        var curYear = (new Date()).getFullYear();
        return [curYear, curYear + 1];
    }
    var getThreeYearsCurMid = function() {
        var curYear = (new Date()).getFullYear();
        return [curYear - 1, curYear, curYear + 1];
    }

    var generateConsecutiveArray = function(cantInspecciones) {
        var indexes = [];
        for (var i = 0; i < cantInspecciones; i++) {
            indexes.push(i + 1);
        }
        return indexes;
    }

    var getMonth = function(index) {
            var months = getMonths();
            return months[parseInt(index) - 1];
    }

    var getPanelClass = function(index) {
        var arr = ['panel-success', 'panel-warning', 'panel-danger'];
        return arr[index - 1];
    }
    var getNivelRiesgo = function(index) {
        var arr = ['BAJO', 'MEDIO', 'ALTO'];
        return arr[index - 1];
    }
    /* no es muy util, use the next instead */
    var uniqueID2 = function() {
        var randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
        var uniqid = randLetter + Date.now();
        return uniqid;
    }

    var uniqueID = function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = crypto.getRandomValues(new Uint8Array(1))[0]%16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }

    var fecha = function (date) {
        this.day = date.getDate();
        this.month = date.getMonth() + 1;
        this.year = date.getFullYear();
        this.toString = function() {
            return this.day + "/" + this.pad(this.month, 2, '0') + "/" + this.year;
        };
        this.pad = function(n, width, z) {
          z = z || '0';
          n = n + '';
          return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }
    }

    var pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    // Validates that the input string is a valid date formatted as "mm/dd/yyyy"
    var isValidDate = function (dateString) {
        // First check for the pattern
        if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
            return false;

        // Parse the date parts to integers
        var parts = dateString.split("/");
        var day = parseInt(parts[0], 10);
        var month = parseInt(parts[1], 10);
        var year = parseInt(parts[2], 10);

        // Check the ranges of month and year
        if(year < 1000 || year > 3000 || month == 0 || month > 12)
            return false;

        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

        // Adjust for leap years
        if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            monthLength[1] = 29;

        // Check the range of the day
        return day > 0 && day <= monthLength[month - 1];
    };

    var getTextID = function(selectorID) {
        /*$($0).is('input')
        $($0).is('textarea')
        $($0).attr('type')*/
        var el = $(selectorID);
        var esinput = el.is('input');
        var inputType = ['radio', 'checkbox', 'text', 'hidden'];
        if (esinput) {
            var index = -1;
            for (var i = 0; i < inputType.length; i++) {
                if (el.attr("type") == inputType[i]) {
                    index = i;
                    break;
                }
            };
            switch(inputType[index]) {
                case 'radio':
                case 'checkbox':
                    return el.is(':checked') ? 1 : 0;
                case 'text':
                case 'hidden':
                    return el.val();
            }
        }
        else {
            /* puede ser select o textarea */
            if (el.is('textarea'))
                return el.val();
            else {
                /* es select */
                var selected = el.find(':selected');
                if (selected.length > 0)
                    return el.find(':selected').text().trim();
                else
                    /* es un elemento html */
                    return el.text();
            }
        }
    }

    var setSelectAllOnFocus = function(selector) {
        $(selector).focus(function() {

            var $this = $(this);

            $this.select();

            window.setTimeout(function() {
                $this.select();
            }, 1);

            // Work around WebKit's little problem
            function mouseUpHandler() {
                // Prevent further mouseup intervention
                $this.off("mouseup", mouseUpHandler);
                return false;
            }

            $this.mouseup(mouseUpHandler);

        });
    }
    /*Enteros*/
    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
           return false;
        return true;
    }
    function isNumeric(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }

    var deepClone = function (item) {
      if (Array.isArray(item)) {
        var newArr = [];

        for (var i = item.length; i-- !== 0;) {
          newArr[i] = deepClone(item[i]);
        }

        return newArr;
      }
      else if (typeof item === 'function') {
        eval('var temp = ' + item.toString());
        return temp;
      }
      else if (typeof item === 'object')
        return Object.create(item);
      else
        return item;
    }

    var formatBytes = function (bytes, decimals) {
       if(bytes == 0) return '0 Byte';
       var k = 1024;
       var dm = decimals + 1 || 3;
       var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
       var i = Math.floor(Math.log(bytes) / Math.log(k));
       return (bytes / Math.pow(k, i)).toPrecision(dm) + ' ' + sizes[i];
    }

    var extractToContext = function(options, context) {
        for (prop in options) {
            if (options.hasOwnProperty(prop)) {
                context[prop] = options[prop];
            }
        }
    }

    /*Fin enteros*/
    return {
        extractToContext : extractToContext,
        formatBytes : formatBytes,
        deepClone : deepClone,
        getThreeYearsCurMid : getThreeYearsCurMid,
        readCookie : readCookie,
        getMonths : getMonths,
        getTwoYearAbove : getTwoYearAbove,
        generateConsecutiveArray : generateConsecutiveArray,
        getMonth : getMonth,
        isNumeric : isNumeric,
        processCustomMessages : processCustomMessages,
        pad : pad,
        getPanelClass : getPanelClass,
        uniqueID : uniqueID,
        fecha: fecha,
        isValidDate : isValidDate,
        getTextID : getTextID,
        setSelectAllOnFocus: setSelectAllOnFocus,
        toTitleCase: toTitleCase,
    };
});
