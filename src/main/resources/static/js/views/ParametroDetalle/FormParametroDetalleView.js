define([
    'mods/modUtils',
    'models/ParametroDetalleModel',
    'text!templates/ParametroDetalle/formParametroDetalleTemplate.html'
    ],
    function(
        modUtils,
        ParametroDetalleModel,
        formParametroDetalleTemplate
    ) {
    var FormParametroDetalleView = Backbone.View.extend({
        initialize : function(options) {
            modUtils.extractToContext(options, this);
            this.render();
            console.log('si entra a initialize de FormParametroDetalleView');
        },
        template : _.template(formParametroDetalleTemplate),
        render : function() {
            this.$el.html(this.template({
                el : '#container-parametro-detalle',
                idParametro: this.parent.idParametro,
                msgHeaderParametroDetalle : this.msgHeaderParametroDetalle,
                tableName : this.parent.tableName,
                detalle : this.detalle,
            }));
            return this;
        },
        events : {
            'click .cancel' : 'doCancel',
            'click .save' : 'doSave',
        },
        doCancel : function() {
            console.log('se hizo click en doCancel');
            this.parent.render();
            this.close();
            // this.parent.parametroDetalleCollection.fetchByIdParametro({idparametro : this.parent.idParametro});
        },
        doSave : function(e) {
            var idParametroDetalle = this.$("#idParametroDetalle").val();
            var parametroDetalleModel, msg = 'creado';
            if (this.parent.detalle) {
                parametroDetalleModel = this.parent.detalle;
                // parametroDetalleModel.set('idParametroDetalle', idParametroDetalle);
                msg = 'actualizado';
            }
            else {
                parametroDetalleModel = new ParametroDetalleModel();
            }

            var descripcion = this.$("#descripcion").val();
            var abreviatura = this.$("#abreviatura").val();
            var ordenPosicion = this.$("#ordenPosicion").val();

            parametroDetalleModel.set('descripcion', descripcion);
            parametroDetalleModel.set('abreviatura', abreviatura);
            parametroDetalleModel.set('ordenPosicion', ordenPosicion == '' ? 0 : ordenPosicion);
            parametroDetalleModel.set('parametro', {idParametro : this.parent.idParametro});
            var self = this;
            console.log('parametroDetalleModel.toJSON(): ', parametroDetalleModel.toJSON());
            parametroDetalleModel.save({}, {
                beforeSend : function(jqXHR, settings) {
                        console.log('settings.data: ', settings.data);
                },
                success : function(model, response, options) {
                    $.notify('se ha ' + msg + ' el registro con éxito', 'success');
                    if (self.parent.detalle) {
                        self.parent.detalle = null;
                        self.parent.parametroDetalleCollection.sort();
                    } else {
                        self.parent.parametroDetalleCollection.add(parametroDetalleModel);
                    }
                    self.close();
                },
                error : function(model, response, options) {
                }
            });

            console.log('se hizo click en doSave');
        },
        onClose : function() {
        },
    })
    return FormParametroDetalleView;
})