define([
    'underscore',
    'backbone',
    'JSOG',
    'bootbox',
    'tablednd',
    'mods/modUtils',
    'mods/modAjax',

    'mods/modCamposDinamicos',
    'collections/ParametroDetalleCollection',
    'models/ParametroDetalleModel',
    'views/ParametroDetalle/FormParametroDetalleView',

    'text!templates/ParametroDetalle/parametroDetalleTemplate.html',
    'text!templates/loadingTemplate.html',
    ], function(
        _,
        Backbone,
        JSOG,
        bootbox,
        tablednd,
        modUtils,
        modAjax,

        modCamposDinamicos,
        ParametroDetalleCollection,
        ParametroDetalleModel,
        FormParametroDetalleView,

        parametroDetalleTemplate,
        loadingTemplate
    ) {

    var ParametroDetalleView = Backbone.View.extend({
        loading : true,
        initialize : function(options) {
            // console.log('options: ', options);
            // console.log('se ha llamado a initialize de ParametroDetalleView');
            modUtils.extractToContext(options, this);
            this.parametroDetalleCollection = new ParametroDetalleCollection();
            this.parametroDetalleCollection.on('sync', this.render, this);
            // console.log('this.idParametro: ', this.idParametro);
            this.parametroDetalleCollection.fetchByIdParametro({idparametro : this.idParametro});
            this.render();
        },
        template : _.template(parametroDetalleTemplate),
        render : function() {
            this.$el.removeClass('hidden');
            if (this.loading) {
                this.$el.html(_.template(loadingTemplate, {}));
                this.loading = false;
                return;
            }
            // this.parametroDetalleCollection.off('sync');
            var self = this;
            var items = JSOG.decode(this.parametroDetalleCollection.toJSON());
            console.log('items: ', items);
            this.$el.html(this.template({
                detalles : items,
                tableName : this.tableName,
                counter : 1,
            }));
            this.$('#tabla-detalle').tableDnD({
                onDragClass: "myDragClass",
                onDrop: function(table, row) {
                    var rows = table.tBodies[0].rows;
                    // var debugStr = "Row dropped was "+row.id+". New order: ";
                    var updateOrders = [];
                    for (var i = 0; i< rows.length; i++) {
                        // console.log('rows[i]: ', rows[i]);
                        updateOrders.push({
                            'idParametroDetalle': rows[i].getAttribute('data-id-parametro-detalle'),
                            'orden': i + 1
                        });
                        // debugStr += rows[i].id+" ";
                    }
                    // console.log('updateOrders: ', updateOrders);
                    // console.log('debugStr: ', debugStr);
                    modAjax.callAJAXPostJSON({
                        url: '/rest/parametrodetalle/bulkupdate',
                        jsonVars : updateOrders,
                        success : function() {
                            console.log('entró a success');
                            self.parametroDetalleCollection.fetchByIdParametro({idparametro : self.idParametro});
                        }
                    });
                },
                onDragStart: function(table, row) {
                   // console.log("Started dragging row "+row.id);
                }
            });
            return this;
        },
        events : {
            'click #add-new' : 'doAdd',
            'click .btn-update' : 'doUpdate',
            'click .btn-delete' : 'doDelete',
            'click .btn-cancel' : 'doCancel',
        },
        doAdd : function(e) {
            e.preventDefault();
            if (this.formParametroDetalleView)
                this.formParametroDetalleView.close();
            this.formParametroDetalleView = new FormParametroDetalleView({
                msgHeaderParametroDetalle : 'Nuevo',
                parent : this,
                detalle: null
            });
            this.$el.addClass('hidden');
            this.parent.$('#' + this.idContainerForm).html(this.formParametroDetalleView.el);
            this.formParametroDetalleView.$('#descripcion').focus().select();
        },
        doUpdate : function(e) {
            var idParametroDetalle = this.getIdParametroDetalle(e);
            this.detalle = this.parametroDetalleCollection.get(idParametroDetalle);
            console.log('detalle: ', this.detalle);

            if (this.formParametroDetalleView)
                this.formParametroDetalleView.close();
            this.formParametroDetalleView = new FormParametroDetalleView({
                msgHeaderParametroDetalle : 'Actualizar',
                parent : this,
                detalle : this.detalle.toJSON(),
            });
            this.$el.addClass('hidden');
            this.parent.$('#' + this.idContainerForm).html(this.formParametroDetalleView.el);
            this.formParametroDetalleView.$('#descripcion').focus().select();
        },
        doDelete : function(e) {
            var curElement = this.$(e.currentTarget);
            var text = curElement.text().trim();
            console.log('text: ', text);
            console.log('text.length: ', text.length);
            if (text == "Eliminar") {
                curElement.text('¿Seguro? - Si');
                curElement.closest('td').find('button.hidden').removeClass('hidden');
                return;
            }

            var self = this;
            var idParametroDetalle = this.getIdParametroDetalle(e);
            parametroDetalleModel = this.parametroDetalleCollection.get(idParametroDetalle);
            parametroDetalleModel.destroy({
                success : function(model, response) {
                    console.log('model: ', model);
                    console.log('response: ', response);
                    if (!response) {
                        bootbox.alert('<h3 class="text-center">no se ha podido completar la operación,' +
                            ' existen registros que dependen de este valor,' +
                            ' elimine dichos registros primero</h4>');
                    } else {
                        $.notify('se ha eliminado el registro con éxito', 'success');
                        console.log('retornó true');
                    }
                    self.parametroDetalleCollection.trigger('sync');
                },
                error : function(model, response) {
                    console.log('error!!!');
                    console.log('model: ', model);
                    console.log('response: ', response);
                }
            });
        },
        doCancel : function(e) {
            var curElement = this.$(e.currentTarget);
            curElement.closest('td').find('button.btn-delete').text("Eliminar");
            curElement.addClass('hidden');
        },
        getIdParametroDetalle(e) {
            return $(e.currentTarget)
                    .closest('tr')
                    .attr('data-id-parametro-detalle');
        },
        onClose : function() {
            if (this.formParametroDetalleView) {
                this.formParametroDetalleView.close();
            }
        }
    })

    return ParametroDetalleView;
});