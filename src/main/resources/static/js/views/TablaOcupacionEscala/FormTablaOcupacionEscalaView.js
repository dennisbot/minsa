define([
    'mods/modUtils',
    'maskedinput',
    'models/TablaOcupacionEscalaModel',
    'text!templates/TablaOcupacionEscala/formTablaOcupacionEscalaTemplate.html'
    ],
    function(
        modUtils,
        maskedinput,
        TablaOcupacionEscalaModel,
        formTablaOcupacionEscalaTemplate
    ) {
    var FormTablaOcupacionEscalaView = Backbone.View.extend({
        initialize : function(options) {
            modUtils.extractToContext(options, this);
            this.render();
            // console.log('test');
        },
        template : _.template(formTablaOcupacionEscalaTemplate),
        render : function() {
            this.$el.html(this.template({
                msgHeaderTablaOcupacionEscala : this.msgHeaderTablaOcupacionEscala,
                tablaOcupacionEscala : this.tablaOcupacionEscala,
            }));
            var mask = "9";
            this.$('#remuneracion').mask("?" + mask.repeat(10), { placeholder : "" } );
            return this;
        },
        events : {
            'click .cancel' : 'doCancel',
            'click .save' : 'doSave',
        },
        doCancel : function() {
            this.parent.render();
            this.close();
        },
        doSave : function(e) {
            var idTablaOcupacionEscala = parseInt(this.$("#idTablaOcupacionEscala").val());
            var tablaOcupacionEscalaModel, msg = 'creado';
            if (idTablaOcupacionEscala < 0) {
                tablaOcupacionEscalaModel = new TablaOcupacionEscalaModel();
                tablaOcupacionEscalaModel.set('parametroDetalleByIdPeriodoEscala',
                    {
                        idParametroDetalle : this.parent.options.params.idPeriodo
                    });
                tablaOcupacionEscalaModel.set('parametroDetalleByIdEscala',
                    {
                        idParametroDetalle : this.parent.options.params.idEscala
                    });
                tablaOcupacionEscalaModel.set('parametroDetalleByIdOcupacionCargoPersonal',
                    {
                        idParametroDetalle : this.tablaOcupacionEscala.
                                            parametroDetalleByIdOcupacionCargoPersonal.
                                            idParametroDetalle,
                        descripcion : this.tablaOcupacionEscala.
                                            parametroDetalleByIdOcupacionCargoPersonal.
                                            descripcion
                });
                tablaOcupacionEscalaModel.set('ordenPosicion', this.tablaOcupacionEscala.
                                            parametroDetalleByIdOcupacionCargoPersonal.
                                            ordenPosicion);
            }
            else {
                msg = 'actualizado';
                tablaOcupacionEscalaModel = this.parent.tablaOcupacionEscala;
            }

            var remuneracion = this.$("#remuneracion").val();

            tablaOcupacionEscalaModel.set('remuneracion', remuneracion);

            var self = this;
            gg = tablaOcupacionEscalaModel;
            if (tablaOcupacionEscalaModel.isValid()) {
                tablaOcupacionEscalaModel.save({}, {
                    beforeSend : function(jqXHR, settings) {
                            // console.log('settings.data: ', settings.data);
                    },
                    success : function(model, response, options) {
                        $.notify('se ha ' + msg + ' el registro con éxito', 'success');
                        if (idTablaOcupacionEscala < 0) {
                            self.parent.tablaOcupacionEscalaCollection.remove(idTablaOcupacionEscala);
                            self.parent.tablaOcupacionEscalaCollection.add(tablaOcupacionEscalaModel);
                        }
                        self.close();
                    },
                    error : function(model, response, options) {
                        console.log('error!');
                        console.log('response: ', response);
                    }
                });
            } else {
                this.showError(tablaOcupacionEscalaModel.validationError);
            }
        },
        showError : function(errors) {
            console.log('errors: ', errors);
            _.each(errors, function(error) {
                this.$('#' + error.name).closest('div.form-group').addClass('has-error');
                this.$('#help-' + error.name).text(error.message);
                this.$('#' + error.name).focus().select();
            }, this);
        },
        onClose : function() {
        },
    })
    return FormTablaOcupacionEscalaView;
})