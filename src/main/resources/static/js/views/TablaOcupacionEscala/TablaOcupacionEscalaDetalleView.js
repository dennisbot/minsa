define([
    'mods/modUtils',
    'mods/modAjax',
    'collections/TablaOcupacionEscalaCollection',
    'views/TablaOcupacionEscala/FormTablaOcupacionEscalaView',
    'text!templates/TablaOcupacionEscala/TablaOcupacionEscalaDetalleTemplate.html',
    'text!templates/loadingTemplate.html',
    ],
    function(
        modUtils,
        modAjax,
        TablaOcupacionEscalaCollection,
        FormTablaOcupacionEscalaView,
        TablaOcupacionEscalaDetalleTemplate,
        loadingTemplate
    ) {
    var TablaOcupacionEscalaDetalleView = Backbone.View.extend({
        loading: true,
        initialize : function(options) {
            this.options = options;
            this.tablaOcupacionEscalaCollection = new TablaOcupacionEscalaCollection();
            this.tablaOcupacionEscalaCollection.on('sync', this.render, this);
            this.tablaOcupacionEscalaCollection.fetchEscalasRemunerativas(options.params);
            this.render();
        },
        template : _.template(TablaOcupacionEscalaDetalleTemplate),
        render : function() {
            this.$el.removeClass('hidden');
            var self = this;
            if (this.loading) {
                this.$el.html(_.template(loadingTemplate, {}));
                this.loading = false;
                return this;
            }
            var items = this.tablaOcupacionEscalaCollection.toJSON();
            // console.log('items: ', items);
            this.$el.html(this.template({
                rows : items,
                counter : 1
            }));
            this.$('#tabla-detalle').tableDnD({
                onDragClass: "myDragClass",
                onDrop: function(table, row) {
                    var rows = table.tBodies[0].rows;
                    // var debugStr = "Row dropped was "+row.id+". New order: ";
                    var updateOrders = [];
                    for (var i = 0; i< rows.length; i++) {
                        updateOrders.push({
                            'idParametroDetalle': rows[i].getAttribute('data-id-parametro-detalle'),
                            'orden': i + 1
                        });
                        // debugStr += rows[i].id+" ";
                    }
                    // console.log('updateOrders: ', updateOrders);
                    // console.log('debugStr: ', debugStr);
                    modAjax.callAJAXPostJSON({
                        url: '/rest/parametrodetalle/bulkupdate',
                        jsonVars : updateOrders,
                        success : function() {
                            self.tablaOcupacionEscalaCollection.fetchEscalasRemunerativas(self.options.params);
                        }
                    });
                }
            });
            return this;
        },
        events : {
            'click .btn-update' : 'doUpdate',
        },
        doUpdate : function(e) {
            var idTablaOcupacionEscala = this.getIdTablaOcupacionEscala(e);
            this.tablaOcupacionEscala = this
            .tablaOcupacionEscalaCollection
            .get(idTablaOcupacionEscala);

            if (this.formTablaOcupacionEscalaView)
                this.formTablaOcupacionEscalaView.close();

            this.$el.addClass('hidden');
            this.formTablaOcupacionEscalaView = new FormTablaOcupacionEscalaView({
                msgHeaderTablaOcupacionEscala : 'Actualizar',
                parent : this,
                tablaOcupacionEscala : this.tablaOcupacionEscala.toJSON(),
            });
            this.options.parent.$('#' + this.options.idFormContainer).html(this.formTablaOcupacionEscalaView.el);
            this.formTablaOcupacionEscalaView.$('#remuneracion').focus().select();
        },
        getIdTablaOcupacionEscala(e) {
            return $(e.currentTarget)
                    .closest('tr')
                    .attr('data-id-tabla-ocupacion-escala');
        },
        onClose : function() {
            if (this.formTablaOcupacionEscalaView)
                this.formTablaOcupacionEscalaView.close();
        }
    })
    return TablaOcupacionEscalaDetalleView;
})