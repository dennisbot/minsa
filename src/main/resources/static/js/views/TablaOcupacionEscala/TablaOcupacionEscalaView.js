define([
        'mods/modUtils',
        'mods/modCamposDinamicos',

        'collections/TablaOcupacionEscalaCollection',
        'views/TablaOcupacionEscala/TablaOcupacionEscalaDetalleView',

        'text!templates/TablaOcupacionEscala/TablaOcupacionEscalaTemplate.html',
        'text!templates/loadingTemplate.html',
    ],
    function(
        modUtils,
        modCamposDinamicos,

        TablaOcupacionEscalaCollection,
        TablaOcupacionEscalaDetalleView,

        TablaOcupacionEscalaTemplate,
        loadingTemplate
    ) {
    var TablaOcupacionEscalaView = Backbone.View.extend({
        loading : true,
        idTableContainer : 'container-tabla-ocupacion-escala',
        idFormContainer : 'container-form',
        initialize : function(options) {
            modUtils.extractToContext(options, this);
            this.tablaOcupacionEscalaCollection = new TablaOcupacionEscalaCollection();
            this.tablaOcupacionEscalaCollection.on('sync', this.render, this);
            this.tablaOcupacionEscalaCollection.fetchEscalasRemunerativasViewParams({});
            this.render();
        },
        template : _.template(TablaOcupacionEscalaTemplate),
        render : function() {
            if (this.loading) {
                this.$el.html(_.template(loadingTemplate, {}));
                this.loading = false;
                return;
            }
            var items = this.tablaOcupacionEscalaCollection.first().toJSON();
            console.log('items: ', items);
            this.$el.html(this.template({
                periodos : items['periodos'],
                escalas : items['escalas'],
                idTableContainer : this.idTableContainer,
                idFormContainer : this.idFormContainer
            }));
            modCamposDinamicos.iniciarChosen({
                selector : '#select-periodo, #select-escala'
            });
            return this;
        },
        events : {
            'change #select-periodo, #select-escala' : 'onChangeGetTable'
        },
        onChangeGetTable : function() {
            var idPeriodo = this.$('#select-periodo').val();
            var idEscala = this.$('#select-escala').val();
            if (idPeriodo && idEscala) {
                if (this.tablaOcupacionEscalaDetalleView)
                    this.tablaOcupacionEscalaDetalleView.close();
                this.tablaOcupacionEscalaDetalleView = new TablaOcupacionEscalaDetalleView({
                    parent : this,
                    idFormContainer : this.idFormContainer,
                    params : {
                        idPeriodo : idPeriodo,
                        idEscala : idEscala,
                    },
                });
                this.$('#' + this.idTableContainer).html(this.tablaOcupacionEscalaDetalleView.el);
            }
        },
        onClose : function() {
            if (this.tablaOcupacionEscalaDetalleView)
                this.tablaOcupacionEscalaDetalleView.close();
        }
    })
    return TablaOcupacionEscalaView;
})