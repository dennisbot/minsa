define([
    'underscore',
    'backbone',

    'JSOG',
    'bootbox',
    'maskedinput',
    'mods/modUtils',

    'mods/modCamposDinamicos',
    'collections/ParametroCollection',
    'views/ParametroDetalle/ParametroDetalleView',

    'text!templates/Parametro/parametroTemplate.html',
    'text!templates/loadingTemplate.html',
    ], function(
        _,
        Backbone,

        JSOG,
        bootbox,
        maskedinput,
        modUtils,

        modCamposDinamicos,
        ParametroCollection,
        ParametroDetalleView,

        parametroTemplate,
        loadingTemplate
    ) {

    var PersonalSaludView = Backbone.View.extend({
        idContainerDetails : 'container-parametro-detalle',
        idContainerForm : 'container-parametro-detalle-form',
        loading : true,
        initialize : function(options) {
            $('#custom-css').empty();
            modUtils.extractToContext(options, this);
            this.parametroCollection = new ParametroCollection();
            this.parametroCollection.on('sync', this.render, this);
            this.parametroCollection.fetch({});
            this.render();
        },
        template : _.template(parametroTemplate),
        render : function() {
            if (this.loading) {
                this.$el.html(_.template(loadingTemplate, {}));
                this.loading = false;
                return;
            }
            var self = this;
            // console.log('this.parametroCollection.models: ', this.parametroCollection.models);
            // console.log('this.parametroCollection.toJSON(): ', this.parametroCollection.toJSON());
            var items = JSOG.decode(this.parametroCollection.toJSON());
            // console.log('items: ', items);
            this.$el.html(this.template({
                parametros : items,
                idContainerDetails : this.idContainerDetails,
                idContainerForm : this.idContainerForm
            }));

            modCamposDinamicos.iniciarChosen({
                selector : '#select-tabla'
            });
            return this;
        },
        events : {
            'click .cancel' : 'doCancel',
            'click .save' : 'doSave',
            'change #select-tabla' : 'doChangeSelectTabla',
        },
        doChangeSelectTabla : function(e) {
            // console.log('e.currentTarget: ', e.currentTarget);
            this.idParametro = $(e.currentTarget).val();
            this.tableName = $(e.currentTarget).find("option:selected").text();
            if (this.parametroDetalleView)
                this.parametroDetalleView.close();
            this.parametroDetalleView = new ParametroDetalleView({
                                        idParametro: this.idParametro,
                                        tableName : this.tableName,
                                        idContainerForm : this.idContainerForm,
                                        parent : this
                                    });
            this.$('#' + this.idContainerDetails).html(this.parametroDetalleView.el);
            // console.log('this.idParametro: ', this.idParametro);
        },
        onClose : function() {
            if (this.parametroDetalleView)
                this.parametroDetalleView.close();
        }
    })

    return PersonalSaludView;
});