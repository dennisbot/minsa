define([
    'underscore',
    'backbone',
    'JSOG',
    'bootbox',
    'maskedinput',
    'mods/modUtils',
    'mods/validaciones/modValidator',
    'mods/validaciones/PersonalSaludValidator',

    'mods/modCamposDinamicos',
    'collections/ParametrosCollection',
    'models/PersonalSaludModel',

    'views/Util/SearchPersonalSaludView',
    'text!templates/PersonalSalud/personalSaludTemplate.html',
    'text!templates/loadingTemplate.html',
    ], function(
        _,
        Backbone,
        JSOG,
        bootbox,
        maskedinput,
        modUtils,
        modValidator,
        PersonalSaludValidator,

        modCamposDinamicos,
        ParametrosCollection,
        PersonalSaludModel,

        SearchPersonalSaludView,
        personalSaludTemplate,
        loadingTemplate
    ) {

    var PersonalSaludView = Backbone.View.extend({
        loading : true,
        initialize : function(options) {
            $('#custom-css').empty();
            modUtils.extractToContext(options, this);
            this.parametros = new ParametrosCollection();
            this.parametros.on('sync', this.parse2render, this);
            this.getParametros();
            this.curSearchHotKey = _.bind(this.searchHotKey, this);
            document.addEventListener('keydown', this.curSearchHotKey, false);
            this.render();
        },
        searchHotKey : function(e) {
            var thechar = String.fromCharCode(e.keyCode);
            /* si (alt + p) -> dosearch */
            if (e.altKey && e.keyCode == 80) {
                this.doSearch(e);
            }
        },
        getParametros : function() {
            this.editPersonalSalud = null;
            this.parametros.fetchPersonalSaludViewParams({});
        },
        parse2render : function() {
            var params = this.parametros.first();
            this.params = {
                personalesSalud : JSOG.decode(params.attributes.personalesSalud),
                tiposDocumentos : params.attributes.tiposDocumentos,
                profesiones : params.attributes.profesiones,
            };
            this.render();
        },
        template : _.template(personalSaludTemplate),
        render : function(editPersonalSalud) {
            if (this.loading) {
                this.$el.html(_.template(loadingTemplate, {}));
                this.loading = false;
                return;
            }
            this.editPersonalSalud = editPersonalSalud;

            var self = this;

            this.$el.html(this.template({
                personalSalud : editPersonalSalud ? editPersonalSalud.toJSON() : null,
                tiposDocumentos : this.params.tiposDocumentos,
                profesiones : this.params.profesiones,
                msgHeaderPersonal : editPersonalSalud ? "ACTUALIZAR" : "AÑADIR NUEVO",
            }));

            modCamposDinamicos.iniciarChosen({
                selector : '#select-tipo-documento'
            });
            modCamposDinamicos.iniciarChosen({
                selector : '#select-profesion'
            });


            modCamposDinamicos.iniciarDatePicker({
                selector : '.datepicker',
                setCurDate : editPersonalSalud ? false : true
            });
            // var maskMoneyParams = {
            //     precision: 0,
            //     defaultZero: false,
            //     thousands: '',
            //     decimal: ''
            // };
            var mask = "9";
            this.$('#nroDocumento').mask("?" + mask.repeat(20), { placeholder : "" } );
            this.$('#celular').mask("?" + mask.repeat(20), { placeholder : "" } );
            /*  enfocamos en el textbox "nombres" */
            this.$('#nombres').focus();
            return this;
        },
        events : {
            'click .cancel' : 'doCancel',
            'click .save' : 'doSave',
            'click .search-personal-salud' : 'doSearch',
            'blur #nombres' : 'doAutocomplete',
            'blur #paterno' : 'doAutocomplete',
        },
        doAutocomplete : function() {
            var nombres = this.$('#nombres').val();
            var paterno = this.$('#paterno').val();
            var curUsuario = this.$('#usuario').val();
            if (nombres != "" && paterno != "" && curUsuario == "") {
                this.$('#usuario').val((nombres.charAt(0) + paterno.replaceAll(" ", "")).toLowerCase());
                this.$('#correo').val((nombres.substr(0,
                                nombres.indexOf(" ") == -1 ? nombres.length : nombres.indexOf(" ")) + '.' +
                                paterno.replaceAll(" ", "")).toLowerCase() + '@mmg.com');
            }
        },
        doCancel : function(e) {
            e.preventDefault();
            this.render();
            // window.location.replace(BASE_URL);
        },
        doSave : function(e) {
            e.preventDefault();
            var self = this;
            var personalSaludModel;

            if (this.editPersonalSalud)
                personalSaludModel = this.editPersonalSalud;
            else
                personalSaludModel = new PersonalSaludModel();

            personalSaludModel.set({
                nombres : this.$('#nombres').val(),
                paterno : this.$('#paterno').val(),
                materno : this.$('#materno').val(),
                usuario : this.$('#usuario').val(),
                nroDocumento : this.$('#nroDocumento').val(),
                parametroDetalleByIdTipoDocumento : {
                    idParametroDetalle : this.$('#select-tipo-documento').val(),
                },
                parametroDetalleByIdProfesion : {
                    idParametroDetalle : this.$('#select-profesion').val(),
                },
                email : this.$('#email').val(),
                celular : this.$('#celular').val() == "" ? null : this.$('#celular').val(),
                fechaNacimiento : this.$('#fechaNacimiento')
                                        .data('datepicker')
                                        .getFormattedDate(),
                                        // .getFormattedDate('yyyy-mm-dd'),
                domicilio : this.$('#domicilio').val(),
                observaciones : this.$('#observaciones').val(),
            });

            var update = false, msg = "creado";
            var idPersonalSalud = this.$('#idPersonalSalud').val();
            if (idPersonalSalud != '') {
                update = true;
                personalSaludModel.set('idPersonalSalud', idPersonalSalud);
                msg = "actualizado";
            }

            var hasMessages = modValidator.validar(
                new PersonalSaludValidator(),
                personalSaludModel.toJSON()
            )

            var valid = (hasMessages == true) ? hasMessages : false;

            if (!valid) {
                this.$('.save').notify(modValidator.showErrorMessages(hasMessages), {
                    position : 'left top',
                    className : 'error'
                })
            }
            else {
                personalSaludModel.on("sync", this.getParametros, this);
                /* first parameter, object with properties and values */
                personalSaludModel.save({}, {
                    beforeSend : function(jqXHR, settings) {
                        // console.log('settings.data: ', settings.data);
                    },
                    success : function(model, response, options) {
                        $.notify('se ha ' + msg + ' el personal de salud exitosamente', 'success');
                    },
                    error : function(model, response, options) {
                        console.log('response: ', response);
                        $.notify('ERROR conectándose con el servidor', 'error');
                    }
                })
            }
        },
        doSearch : function(e) {
            e.preventDefault();
            /* subview doSearch */
            this.searchView = new SearchPersonalSaludView({
                el : '#modal-area',
                personalesSalud : this.params.personalesSalud,
                parent : this,
                showEdit : true
            });
        },
        onClose : function() {
            document.removeEventListener('keydown', this.curSearchHotKey, false);
            this.parametros.off();
            if (this.searchView)
                this.searchView.close();
        }
    })

    return PersonalSaludView;
});