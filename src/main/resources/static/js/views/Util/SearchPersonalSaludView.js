define([
    'underscore',
    'backbone',
    'JSOG',
    'models/PersonalSaludModel',
    'mods/modUtils',
    'mods/DataTable/DataTable',
    'config/searchPersonalSaludConfig',
    'text!templates/Util/searchPersonalSaludTemplate.html'
    ],
function(
        _,
        Backbone,
        JSOG,
        PersonalSaludModel,
        modUtils,
        DataTable,
        searchPersonalSaludConfig,
        searchPersonalSaludTemplate
) {

    var SearchPersonalSaludView = Backbone.View.extend({
        initialize : function(options) {
            modUtils.extractToContext(options, this);
            this.addCustomColumn();
            this.nombresParaMostrar = _.pluck(this.personalesSalud, 'nombreParaMostrar');
            this.render();
        },
        events : {
            'keyup #search-term' : 'doSearch'
        },
        template : _.template(searchPersonalSaludTemplate),
        /* fuzzy, mejor implementación */
        render : function() {
            var self = this;
            this.$el.html(this.template({}));
            this.$('#myModal').modal().on('shown.bs.modal', function() {
                self.$('#search-term').focus();
                self.dt = new DataTable();
            })
        },
        addCustomColumn : function() {
            if (this.personalesSalud) {
                _.each(this.personalesSalud, function(element, index) {
                    _.extend(element, {
                        nombreParaMostrar: element.nombres + ' '
                        + element.paterno + ' '
                        + element.materno
                    });
                })
            }
            else {
                console.log('no existe el campo = this.personalesSalud: ', this.personalesSalud);
            }
        },
        doSelectSearch : function(e, curItem) {
            this.$('#myModal').modal('hide');
        },
        doEdit : function(curItem) {
            this.personalSaludModel = new PersonalSaludModel({
                idPersonalSalud : curItem.idPersonalSalud
            })
            this.personalSaludModel.on('sync', this.callParentRender, this);
            this.personalSaludModel.fetch();
            // console.log('curItem: ', curItem);
        },
        callParentRender : function() {
            this.doSelectSearch();
            /* quitamos el sync ya que no lo necesitamos más */
            this.personalSaludModel.off('sync');
            this.parent.render(this.personalSaludModel);
        },
        doSearch : function(e) {
            var self = this;
            var options = {
                pre: '<span class="highlight">',
                post: '</span>'
            };
            if (e.altKey || e.ctrlKey || e.shiftKey || e.keyCode == 18 ||
                e.keyCode == 17 || e.keyCode == 13 || e.keyCode == 16
                || e.keyCode == 20 || e.keyCode == 27 || e.keyCode == 9) return false;

            var searchString = $(e.currentTarget).val();
            /* vamos a limpiar el contenido */
            if (searchString.length == 0 && this.dt.datatable) {
                this.dt.datatable.clear();
                this.dt.datatable.columns.adjust().draw();
            }
            /* nos aseguramos que al menos sea longitud de 3 */
            if (searchString.length < 3) return false;

            $('#div-table-search-container').show();
            var results = fuzzy.filter(searchString, self.nombresParaMostrar, options);
            var newCollection = _.map(results, function(result) {
                self.personalesSalud[result.index].descripcionItemResult = result.string;
                return self.personalesSalud[result.index];
            });

            if (this.dt.datatable) {
                this.dt.datatable.clear();
                if (newCollection.length > 0) {
                    this.dt.datatable.rows.add(newCollection);
                }
                this.dt.datatable.columns.adjust().draw();
                this.dt.prepararEdiciones();
            }
            else {
                this.dt
                    .showGrid(newCollection, searchPersonalSaludConfig.getConfig(self));
            }
        },
        onClose : function() {
            this.dt = null;
            if (this.personalSaludModel)
                this.personalSaludModel = null;
        }
    })

    return SearchPersonalSaludView;
});