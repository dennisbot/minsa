define([
        'underscore',
        'text!templates/Util/searchSelectPersonalSaludTemplate.html',
    ],
    function (
        _,
        searchSelectPersonalSaludTemplate
    ) {

    var searchPersonalSaludConfig = {
        getConfig: function(context) {
            // console.log('response: ', response);
            // console.log('entra a getConfig de searchPersonalSaludConfig');
            return {
                rowId: 'idPersonalSalud',
                hasDetails: true,
                paging: true,
                bFilter: false,
                order: [[0, 'asc']],
                container: '#table-result-search-personal-salud',
                $container: $('#table-result-search-personal-salud'),
                doSelect : _.bind(context.doSelectSearch, context),
                doEdit : _.bind(context.doEdit, context),
                columns: [
                    {
                        data : 'descripcionItemResult',
                        // width : '40%',
                    },
                    {
                        data : function(row, type, set) {
                            if (row.parametroDetalleByIdProfesion)
                                return row.parametroDetalleByIdProfesion.descripcion;
                            return '(por definir)';
                        },
                        sClass : 'text-center',
                        // width : '40%',
                    },
                    {
                        data : function(row, type, set) {
                            if (row.contratos && row.contratos.length > 0)
                                return row.contratos[0].tablaOcupacionEscala.parametroDetalleByIdOcupacionCargoPersonal.descripcion;
                            return '(por definir)';
                        },
                        sClass : 'text-center',
                        // width : '40%',
                    },
                    {
                        data : 'idPersonalSalud',
                        width : '30%',
                        sClass : 'text-center',
                        orderable: false,
                    }
                ],
                columnDefs: [
                     {
                        "targets": -1,
                        "data": null,
                        "mRender": function (data, type, row) {
                            if (row.cargo)
                                return _.template(searchSelectPersonalSaludTemplate, { personalSalud : row , showEdit : context.showEdit });
                            var html = context.showEdit ? '<a href="#" class="obj-edit pull-left" title="editar"><i class="fa fa-pencil"></i> Editar</a>' : '';
                            var pr = context.showEdit ? ' clas="pull-right"' : '';
                            return html + '<span' + pr + '>(dependencia por definir)</span>';
                        }
                        // "defaultContent": '<button class="btn btn-default">Click!</button>'
                    },
                ],
                lengthMenu : [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            }
        }
    }

    return searchPersonalSaludConfig;
});