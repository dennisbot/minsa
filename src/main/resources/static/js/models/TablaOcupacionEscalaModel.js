define(['mods/modUtils',
    'JSOG'],
    function(
        modUtils,
        jsog
    ) {
    var TablaOcupacionEscalaModel = Backbone.Model.extend({
        urlRoot: 'rest/tablaocupacionescala',
        idAttribute : 'idTablaOcupacionEscala',
        defaults : {},
        validate : function(attributes) {
            var errors = [];
            if (!modUtils.isNumeric(attributes.remuneracion))
                errors.push({name : 'remuneracion', message : 'ingrese un monto válido'});
            else
                if (parseFloat(attributes.remuneracion) < 0)
                    errors.push({name : 'remuneracion', message : 'ingrese un monto válido'});
            if (errors.length > 0) return errors;
        },
        parse : function(response) {
            return jsog.decode(response);
        }
    })
    return TablaOcupacionEscalaModel;
})