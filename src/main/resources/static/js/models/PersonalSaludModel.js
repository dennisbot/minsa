define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var PersonalSaludModel = Backbone.Model.extend({
        urlRoot: 'rest/personalsalud',
        idAttribute: "idPersonalSalud",
        defaults: {
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
        saveProfile: function(attrs, options) {
          console.log('options: ', options);
          console.log('this.urlRoot: ', this.urlRoot);
          options.url = this.urlRoot + '/updateProfile/:id';
          // Proxy the call to the original save function
          Backbone.Model.prototype.save.call(this, attrs, options);
        }
    });
    // Return the model for the module
    return PersonalSaludModel;
});