define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var CargoModel = Backbone.Model.extend({
        urlRoot: 'rest/cargo',
        idAttribute: "idCargo",
        defaults: {
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
    });
    // Return the model for the module
    return CargoModel;
});