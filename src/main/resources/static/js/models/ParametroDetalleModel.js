define([
  'underscore',
  'backbone'
], function(
        _,
        Backbone
    ) {
    var ParametroDetalleModel = Backbone.Model.extend({
        urlRoot: 'rest/parametrodetalle',
        idAttribute: 'idParametroDetalle',
        defaults: {
        },
        initialize: function(attributes, options) {
            if (options && options.urlRoot)
                this.urlRoot = options.urlRoot;
        },
        saveProfile: function(attrs, options) {
          console.log('options: ', options);
          console.log('this.urlRoot: ', this.urlRoot);
          options.url = this.urlRoot + '/updateProfile/:id';
          // Proxy the call to the original save function
          Backbone.Model.prototype.save.call(this, attrs, options);
        }
    });
    // Return the model for the module
    return ParametroDetalleModel;
});