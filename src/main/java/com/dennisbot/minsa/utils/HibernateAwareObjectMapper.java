package com.dennisbot.minsa.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

@SuppressWarnings("serial")
public class HibernateAwareObjectMapper extends ObjectMapper {
	 public HibernateAwareObjectMapper() {
		 	 
	        registerModule(new Hibernate5Module());
	        SimpleModule module = new SimpleModule();
	        module.addSerializer(Date.class, new JsonDateSerializer());
	        registerModule(module);
	        // habilitar las 3 lineas de abajo en producción (para no pasar valores nulos)
//	        this.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
//	        this.setSerializationInclusion(Include.NON_NULL);
//	        this.setSerializationInclusion(Include.NON_EMPTY);
	        
//	        copiado para referencia /quizá se pueda usar acá
//	        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//			 objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	       
//	        este no se recomienda, el parser no te ayuda, necesita de los quotes en las keys
//	        configure(JsonGenerator.Feature.QUOTE_FIELD_NAMES, false);
//	        configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
	        
//	        this.setSerializationInclusion(Include. non_default?);
//	        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	        setDateFormat(df);
	    }
}
