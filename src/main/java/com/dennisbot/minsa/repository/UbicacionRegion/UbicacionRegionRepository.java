package com.dennisbot.minsa.repository.UbicacionRegion;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dennisbot.minsa.domain.UbicacionRegion;

@Transactional
public interface UbicacionRegionRepository extends JpaRepository<UbicacionRegion, Integer>, UbicacionRegionRepositoryCustomInterface {

}
