package com.dennisbot.minsa.repository.UbicacionRegion;

import com.dennisbot.minsa.domain.UbicacionRegion;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional(readOnly = true)
public class UbicacionRegionRepositoryImpl implements UbicacionRegionRepositoryCustomInterface {
    @PersistenceContext
    EntityManager entityManager;
    @Override
    public Iterable<UbicacionRegion> getFirstNamesLike(String firstName) {
        Query query = entityManager.createNativeQuery("SELECT em.* FROM UbicacionRegion as em " +
                "WHERE em.firstname LIKE ?", UbicacionRegion.class);
        query.setParameter(1, firstName + "%");
        return query.getResultList();
    }
}