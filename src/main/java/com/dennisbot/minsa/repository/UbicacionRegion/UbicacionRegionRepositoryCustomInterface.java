package com.dennisbot.minsa.repository.UbicacionRegion;

import com.dennisbot.minsa.domain.UbicacionRegion;

public interface UbicacionRegionRepositoryCustomInterface {
    Iterable<UbicacionRegion> getFirstNamesLike(String firstName);
}