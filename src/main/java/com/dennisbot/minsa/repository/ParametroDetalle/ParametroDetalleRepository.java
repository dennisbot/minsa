package com.dennisbot.minsa.repository.ParametroDetalle;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dennisbot.minsa.domain.ParametroDetalle;
import com.dennisbot.minsa.domain.custom.UpdateOrder;
import com.dennisbot.minsa.domain.projection.PDetalle;

@Transactional
public interface ParametroDetalleRepository extends JpaRepository<ParametroDetalle, Integer>, ParametroDetalleRepositoryCustomInterface {

	@Query("select new com.dennisbot.minsa.domain.projection.PDetalle(pd.idParametroDetalle, pd.descripcion) from ParametroDetalle pd join pd.parametro p where p.idParametro = :idParametro")
	public Iterable<PDetalle> findByIdParametro(@Param("idParametro") int idParametro);
	@Query("select pd from ParametroDetalle pd join pd.parametro p where p.idParametro = :idParametro ORDER BY pd.ordenPosicion")
	public Iterable<ParametroDetalle> findByIdParametroFull(@Param("idParametro") int idParametro);
}
