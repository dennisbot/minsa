package com.dennisbot.minsa.repository.ParametroDetalle;

import com.dennisbot.minsa.domain.ParametroDetalle;
import com.dennisbot.minsa.domain.custom.UpdateOrder;

import java.util.List;

public interface ParametroDetalleRepositoryCustomInterface {
    List<ParametroDetalle> getFirstNamesLike(String firstName);
    public boolean bulkUpdatePosition(Iterable<UpdateOrder> items);
}