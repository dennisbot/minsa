package com.dennisbot.minsa.repository.ParametroDetalle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dennisbot.minsa.domain.ParametroDetalle;
import com.dennisbot.minsa.domain.custom.UpdateOrder;

@Repository
@Transactional
public class ParametroDetalleRepositoryImpl implements ParametroDetalleRepositoryCustomInterface {
    @PersistenceContext
    EntityManager entityManager;
    @Override
    public List<ParametroDetalle> getFirstNamesLike(String firstName) {
        Query query = entityManager.createNativeQuery("SELECT em.* FROM ParametroDetalle as em " +
                "WHERE em.firstname LIKE ?", ParametroDetalle.class);
        query.setParameter(1, firstName + "%");
        return query.getResultList();
    }
	@Override
	public boolean bulkUpdatePosition(Iterable<UpdateOrder> items) {
		try {
			List<Integer> ids = new ArrayList<Integer>();
			HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
			for (UpdateOrder item : items) {
				ids.add(item.idParametroDetalle);
				map.put(item.idParametroDetalle, item.orden);
			}
			Query query = entityManager.createQuery("SELECT pd FROM ParametroDetalle as pd " +
	                "WHERE pd.idParametroDetalle IN (:idParametroDetalles)", ParametroDetalle.class);
	        query.setParameter("idParametroDetalles", ids);
	        List<ParametroDetalle> detalles = query.getResultList();
	       
	        for (ParametroDetalle detalle : detalles) {
	        	detalle.setOrdenPosicion(map.get(detalle.getIdParametroDetalle()));
	        	String qlString = "UPDATE ParametroDetalle pd SET ordenPosicion = :ordenPos "
	        			+ "WHERE pd.idParametroDetalle = :idParametroDetalle";
	        	query = entityManager.createQuery(qlString);
	        	query.setParameter("ordenPos", detalle.getOrdenPosicion());
	        	query.setParameter("idParametroDetalle", detalle.getIdParametroDetalle());
	        	query.executeUpdate();
	        }
			return true;
		} catch (Exception e) {
			System.out.println("mensaje de error" + e.getMessage());
			return false;
		}
		
	}
}