package com.dennisbot.minsa.repository.OficinaArea;

import com.dennisbot.minsa.domain.OficinaArea;
import java.util.List;

public interface OficinaAreaRepositoryCustomInterface {
    List<OficinaArea> getFirstNamesLike(String firstName);
}