package com.dennisbot.minsa.repository.OficinaArea;

import com.dennisbot.minsa.domain.OficinaArea;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class OficinaAreaRepositoryImpl implements OficinaAreaRepositoryCustomInterface {
    @PersistenceContext
    EntityManager entityManager;
    @Override
    public List<OficinaArea> getFirstNamesLike(String firstName) {
        Query query = entityManager.createNativeQuery("SELECT em.* FROM OficinaArea as em " +
                "WHERE em.firstname LIKE ?", OficinaArea.class);
        query.setParameter(1, firstName + "%");
        return query.getResultList();
    }
}