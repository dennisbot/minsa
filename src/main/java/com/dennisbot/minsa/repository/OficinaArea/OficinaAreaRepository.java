package com.dennisbot.minsa.repository.OficinaArea;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dennisbot.minsa.domain.OficinaArea;

@Transactional
public interface OficinaAreaRepository extends JpaRepository<OficinaArea, Integer>, OficinaAreaRepositoryCustomInterface {

}
