package com.dennisbot.minsa.repository.RedMicroredEess;

import com.dennisbot.minsa.domain.RedMicroredEess;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional(readOnly = true)
public class RedMicroredEessRepositoryImpl implements RedMicroredEessRepositoryCustomInterface {
    @PersistenceContext
    EntityManager entityManager;
    @Override
    public Iterable<RedMicroredEess> getFirstNamesLike(String firstName) {
        Query query = entityManager.createNativeQuery("SELECT em.* FROM RedMicroredEess as em " +
                "WHERE em.firstname LIKE ?", RedMicroredEess.class);
        query.setParameter(1, firstName + "%");
        return query.getResultList();
    }
}