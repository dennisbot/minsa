package com.dennisbot.minsa.repository.RedMicroredEess;

import com.dennisbot.minsa.domain.RedMicroredEess;

public interface RedMicroredEessRepositoryCustomInterface {
    Iterable<RedMicroredEess> getFirstNamesLike(String firstName);
}