package com.dennisbot.minsa.repository.RedMicroredEess;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dennisbot.minsa.domain.RedMicroredEess;

@Transactional
public interface RedMicroredEessRepository extends JpaRepository<RedMicroredEess, Integer>, RedMicroredEessRepositoryCustomInterface {

}
