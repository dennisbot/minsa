package com.dennisbot.minsa.repository.PersonalSalud;

import com.dennisbot.minsa.domain.PersonalSalud;

import org.springframework.beans.BeanUtils;
import org.springframework.boot.autoconfigure.jdbc.metadata.CommonsDbcp2DataSourcePoolMetadata;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import java.util.List;

@Repository
@Transactional(readOnly = true)
public class PersonalSaludRepositoryImpl implements PersonalSaludRepositoryCustomInterface {
    @PersistenceContext
    EntityManager em;
    @Override
    public List<PersonalSalud> getFirstNamesLike(String firstName) {
//        Query query = entityManager.createNativeQuery("SELECT em.* FROM PersonalSalud as em " +
//                "WHERE em.firstname LIKE ?", PersonalSalud.class);
        Query query = em.createNativeQuery("SELECT * FROM personal_salud", PersonalSalud.class);
        return query.getResultList();
    }
	@Override
	public boolean canLogIn(PersonalSalud ps) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<PersonalSalud> criteria = cb.createQuery(PersonalSalud.class);
		Root<PersonalSalud> e = criteria.from(PersonalSalud.class);
//		e.fetch("cargo", JoinType.LEFT);
		criteria.select(e)
		.where(
				cb.equal(e.get("usuario"), ps.getUsuario()),
				cb.equal(e.get("clave"), ps.getClave())
		);
		try {
			PersonalSalud retrieved = em.createQuery(criteria).getSingleResult();
			BeanUtils.copyProperties(retrieved, ps);
			System.out.println("____________________________________________");
			System.out.println(ps.getNombres());
			System.out.println(ps.getPaterno());
			System.out.println(ps.getMaterno());
			System.out.println(ps.getUsuario());
			System.out.println("____________________________________________");
			return retrieved != null;
		} catch (Exception ex) {
			return false;
		}
	}
}