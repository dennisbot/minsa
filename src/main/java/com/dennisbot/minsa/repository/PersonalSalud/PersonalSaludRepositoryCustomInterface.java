package com.dennisbot.minsa.repository.PersonalSalud;

import com.dennisbot.minsa.domain.PersonalSalud;
import java.util.List;

public interface PersonalSaludRepositoryCustomInterface {
    List<PersonalSalud> getFirstNamesLike(String firstName);
    public boolean canLogIn(PersonalSalud ps);
}