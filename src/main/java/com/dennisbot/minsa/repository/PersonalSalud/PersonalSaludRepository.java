package com.dennisbot.minsa.repository.PersonalSalud;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.dennisbot.minsa.domain.PersonalSalud;

@Transactional
public interface PersonalSaludRepository extends JpaRepository<PersonalSalud, Integer>, PersonalSaludRepositoryCustomInterface {
	@Query("select ps from PersonalSalud ps left join fetch ps.contratos c left join fetch c.tablaOcupacionEscala toe left join fetch toe.parametroDetalleByIdOcupacionCargoPersonal")
	@EntityGraph(value = "PersonalSalud.profesion_tipo_documento", type = EntityGraphType.FETCH)
	public Iterable<PersonalSalud> findAllWithOcupacion();
	@EntityGraph(value = "PersonalSalud.profesion_tipo_documento", type = EntityGraphType.FETCH)
	public PersonalSalud findFirstByIdPersonalSalud(Integer idPersonalSalud);
}
