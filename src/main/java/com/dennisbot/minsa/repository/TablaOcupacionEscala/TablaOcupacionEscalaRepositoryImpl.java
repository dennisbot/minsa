package com.dennisbot.minsa.repository.TablaOcupacionEscala;

import com.dennisbot.minsa.domain.TablaOcupacionEscala;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional(readOnly = true)
public class TablaOcupacionEscalaRepositoryImpl implements TablaOcupacionEscalaRepositoryCustomInterface {
    @PersistenceContext
    EntityManager entityManager;
    @Override
    public Iterable<TablaOcupacionEscala> getFirstNamesLike(String firstName) {
        Query query = entityManager.createNativeQuery("SELECT em.* FROM TablaOcupacionEscala as em " +
                "WHERE em.firstname LIKE ?", TablaOcupacionEscala.class);
        query.setParameter(1, firstName + "%");
        return query.getResultList();
    }
	@Override
	public Iterable<TablaOcupacionEscala> 
	getTable(Integer idParametro, Integer idPeriodoEscala, Integer idEscala) {
		Query query = entityManager.createNativeQuery(""
				+ "SELECT IFNULL(toe.id_tabla_ocupacion_escala, @num \\:= @num - 1) id_tabla_ocupacion_escala,  " +
				"toe.id_periodo_escala,  " +
				"IFNULL(toe.id_ocupacion_cargo_personal, oc.id_parametro_detalle) id_ocupacion_cargo_personal, " +
				"toe.id_escala, " +
				"toe.remuneracion, " +
				"oc.orden_posicion, " +
				"oc.descripcion  " +
				"FROM tabla_ocupacion_escala toe RIGHT OUTER JOIN  " +
				"( " +
				"SELECT * FROM parametro_detalle, (SELECT @num \\:= 0) AS n WHERE id_parametro = :id_parametro " +
				" ORDER BY orden_posicion) oc ON toe.id_ocupacion_cargo_personal = oc.id_parametro_detalle " +
				"AND id_periodo_escala = :id_periodo_escala AND id_escala = :id_escala " +
				""
//				, "getTable");
				, TablaOcupacionEscala.class);
        query.setParameter("id_parametro", idParametro);
        query.setParameter("id_periodo_escala", idPeriodoEscala);
        query.setParameter("id_escala", idEscala);
        return query.getResultList();
	}
}