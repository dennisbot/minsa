package com.dennisbot.minsa.repository.TablaOcupacionEscala;

import com.dennisbot.minsa.domain.TablaOcupacionEscala;

public interface TablaOcupacionEscalaRepositoryCustomInterface {
    Iterable<TablaOcupacionEscala> getFirstNamesLike(String firstName);
    Iterable<TablaOcupacionEscala> getTable(Integer idParametro, Integer idPeriodoEscala, Integer idEscala);
}