package com.dennisbot.minsa.repository.TablaOcupacionEscala;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dennisbot.minsa.domain.TablaOcupacionEscala;

@Transactional
public interface TablaOcupacionEscalaRepository extends JpaRepository<TablaOcupacionEscala, Integer>, TablaOcupacionEscalaRepositoryCustomInterface {

}
