package com.dennisbot.minsa.repository.Contrato;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dennisbot.minsa.domain.Contrato;

@Transactional
public interface ContratoRepository extends JpaRepository<Contrato, Integer>, ContratoRepositoryCustomInterface {

}
