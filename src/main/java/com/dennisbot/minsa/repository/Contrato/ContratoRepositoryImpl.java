package com.dennisbot.minsa.repository.Contrato;

import com.dennisbot.minsa.domain.Contrato;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional(readOnly = true)
public class ContratoRepositoryImpl implements ContratoRepositoryCustomInterface {
    @PersistenceContext
    EntityManager entityManager;
    @Override
    public Iterable<Contrato> getFirstNamesLike(String firstName) {
        Query query = entityManager.createNativeQuery("SELECT em.* FROM Contrato as em " +
                "WHERE em.firstname LIKE ?", Contrato.class);
        query.setParameter(1, firstName + "%");
        return query.getResultList();
    }
}