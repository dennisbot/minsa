package com.dennisbot.minsa.repository.Contrato;

import com.dennisbot.minsa.domain.Contrato;

public interface ContratoRepositoryCustomInterface {
    Iterable<Contrato> getFirstNamesLike(String firstName);
}