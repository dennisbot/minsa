package com.dennisbot.minsa.repository.LicenciaPermiso;

import com.dennisbot.minsa.domain.LicenciaPermiso;

public interface LicenciaPermisoRepositoryCustomInterface {
    Iterable<LicenciaPermiso> getFirstNamesLike(String firstName);
}