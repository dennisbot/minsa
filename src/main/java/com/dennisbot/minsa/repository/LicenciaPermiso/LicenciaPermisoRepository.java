package com.dennisbot.minsa.repository.LicenciaPermiso;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dennisbot.minsa.domain.LicenciaPermiso;

@Transactional
public interface LicenciaPermisoRepository extends JpaRepository<LicenciaPermiso, Integer>, LicenciaPermisoRepositoryCustomInterface {

}
