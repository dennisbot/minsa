package com.dennisbot.minsa.repository.LicenciaPermiso;

import com.dennisbot.minsa.domain.LicenciaPermiso;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional(readOnly = true)
public class LicenciaPermisoRepositoryImpl implements LicenciaPermisoRepositoryCustomInterface {
    @PersistenceContext
    EntityManager entityManager;
    @Override
    public Iterable<LicenciaPermiso> getFirstNamesLike(String firstName) {
        Query query = entityManager.createNativeQuery("SELECT em.* FROM LicenciaPermiso as em " +
                "WHERE em.firstname LIKE ?", LicenciaPermiso.class);
        query.setParameter(1, firstName + "%");
        return query.getResultList();
    }
}