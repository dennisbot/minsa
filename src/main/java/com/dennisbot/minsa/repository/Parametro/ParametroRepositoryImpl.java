package com.dennisbot.minsa.repository.Parametro;

import com.dennisbot.minsa.domain.Parametro;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class ParametroRepositoryImpl implements ParametroRepositoryCustomInterface {
    @PersistenceContext
    EntityManager entityManager;
    @Override
    public List<Parametro> getFirstNamesLike(String firstName) {
        Query query = entityManager.createNativeQuery("SELECT em.* FROM Parametro as em " +
                "WHERE em.firstname LIKE ?", Parametro.class);
        query.setParameter(1, firstName + "%");
        return query.getResultList();
    }
}