package com.dennisbot.minsa.repository.Parametro;

import com.dennisbot.minsa.domain.Parametro;
import java.util.List;

public interface ParametroRepositoryCustomInterface {
    List<Parametro> getFirstNamesLike(String firstName);
}