package com.dennisbot.minsa.repository.Parametro;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dennisbot.minsa.domain.Parametro;

@Transactional
public interface ParametroRepository extends JpaRepository<Parametro, Integer>, ParametroRepositoryCustomInterface {
	@Query("select p.idParametro, p.descripcionTabla from Parametro p where p.descripcionTabla = :descripcionTabla")
	public Iterable<Parametro> findIdParametroByDescripcionTabla(@Param("descripcionTabla") String descripcionTabla);
//	esta forma es especial (tienes que seguir las convenciones de nombres)
	public Parametro findTopByDescripcionTabla(String descripcionTabla);
	
}
