package com.dennisbot.minsa.repository.Vacaciones;

import com.dennisbot.minsa.domain.Vacaciones;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional(readOnly = true)
public class VacacionesRepositoryImpl implements VacacionesRepositoryCustomInterface {
    @PersistenceContext
    EntityManager entityManager;
    @Override
    public Iterable<Vacaciones> getFirstNamesLike(String firstName) {
        Query query = entityManager.createNativeQuery("SELECT em.* FROM Vacaciones as em " +
                "WHERE em.firstname LIKE ?", Vacaciones.class);
        query.setParameter(1, firstName + "%");
        return query.getResultList();
    }
}