package com.dennisbot.minsa.repository.Vacaciones;

import com.dennisbot.minsa.domain.Vacaciones;

public interface VacacionesRepositoryCustomInterface {
    Iterable<Vacaciones> getFirstNamesLike(String firstName);
}