package com.dennisbot.minsa.repository.Vacaciones;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dennisbot.minsa.domain.Vacaciones;

@Transactional
public interface VacacionesRepository extends JpaRepository<Vacaciones, Integer>, VacacionesRepositoryCustomInterface {

}
