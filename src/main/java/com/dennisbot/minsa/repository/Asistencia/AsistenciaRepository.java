package com.dennisbot.minsa.repository.Asistencia;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dennisbot.minsa.domain.Asistencia;

@Transactional
public interface AsistenciaRepository extends JpaRepository<Asistencia, Integer>, AsistenciaRepositoryCustomInterface {

}
