package com.dennisbot.minsa.repository.Asistencia;

import com.dennisbot.minsa.domain.Asistencia;

public interface AsistenciaRepositoryCustomInterface {
    Iterable<Asistencia> getFirstNamesLike(String firstName);
}