package com.dennisbot.minsa.repository.Asistencia;

import com.dennisbot.minsa.domain.Asistencia;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional(readOnly = true)
public class AsistenciaRepositoryImpl implements AsistenciaRepositoryCustomInterface {
    @PersistenceContext
    EntityManager entityManager;
    @Override
    public Iterable<Asistencia> getFirstNamesLike(String firstName) {
        Query query = entityManager.createNativeQuery("SELECT em.* FROM Asistencia as em " +
                "WHERE em.firstname LIKE ?", Asistencia.class);
        query.setParameter(1, firstName + "%");
        return query.getResultList();
    }
}