package com.dennisbot.minsa.repository.RolTurno;

import com.dennisbot.minsa.domain.RolTurno;

public interface RolTurnoRepositoryCustomInterface {
    Iterable<RolTurno> getFirstNamesLike(String firstName);
}