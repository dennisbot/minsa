package com.dennisbot.minsa.repository.RolTurno;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.dennisbot.minsa.domain.RolTurno;

@Transactional
public interface RolTurnoRepository extends JpaRepository<RolTurno, Integer>, RolTurnoRepositoryCustomInterface {

}
