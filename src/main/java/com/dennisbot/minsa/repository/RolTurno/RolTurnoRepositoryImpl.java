package com.dennisbot.minsa.repository.RolTurno;

import com.dennisbot.minsa.domain.RolTurno;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Transactional(readOnly = true)
public class RolTurnoRepositoryImpl implements RolTurnoRepositoryCustomInterface {
    @PersistenceContext
    EntityManager entityManager;
    @Override
    public Iterable<RolTurno> getFirstNamesLike(String firstName) {
        Query query = entityManager.createNativeQuery("SELECT em.* FROM RolTurno as em " +
                "WHERE em.firstname LIKE ?", RolTurno.class);
        query.setParameter(1, firstName + "%");
        return query.getResultList();
    }
}