package com.dennisbot.minsa;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.text.StrSubstitutor;

public class GenerateRepository {

	public static String PATH_TEMPLATES = "C:\\dev\\templates_generator";

	public static void main(String[] args) throws IOException {

		boolean proceed = true;

		if (!proceed)
			return;

		boolean modifyEntityFiles = true;
		boolean createRepositoryFiles = true;
		boolean createRestFiles = true;

		GenerateRepository gr = new GenerateRepository();
		String fullPackagePath = gr.getFullPackagePath();
		String packageName = gr.getPackageName();
		System.out.println("packageName: = ");
		System.out.println(packageName);

		File domainPath = new File(fullPackagePath + "\\domain");
		File repositoryPath = new File(fullPackagePath + "\\repository");
		File restPath = new File(fullPackagePath + "\\rest");

		if (!domainPath.exists())
			domainPath.mkdir();
		if (!repositoryPath.exists())
			repositoryPath.mkdir();
		if (!restPath.exists())
			restPath.mkdir();

		if (domainPath.exists() && domainPath.isDirectory()) {
			System.out.println("si es directorio!!!");
			ArrayList<File> domainFiles = new ArrayList<File>(Arrays.asList(domainPath.listFiles()));
			System.out.println("size: " + domainFiles.size());
			File fileAnnotationEntity, fileImportAnnotation;
			for (File file : domainFiles) {
				if (file.getName().startsWith("User") || file.isDirectory())
					continue;
				System.out.println("___________________________________");
				System.out.println(file.getName());
				System.out.println(file.getAbsoluteFile());
				System.out.println(file.getAbsolutePath());
				System.out.println(fullPackagePath);
				System.out.println("___________________________________");

				if (modifyEntityFiles) {
					Path path = Paths.get(file.getAbsolutePath());
					List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
					int importLine = 0, entityLine = 0, i = 0;
					for (String line : lines) {
						if (line.startsWith("import"))
							importLine = i;
						if (line.startsWith("@Entity"))
							entityLine = i;
						i++;
					}

					if (file.getName().startsWith("PersonalSalud")) {
						fileAnnotationEntity = new File(PATH_TEMPLATES + "\\head_jsog_graph_entity.txt");
						fileImportAnnotation = new File(PATH_TEMPLATES + "\\import_jsog_graph_entity.txt");
					}

					else {
						fileAnnotationEntity = new File(PATH_TEMPLATES + "\\head_jsog_entity.txt");
						fileImportAnnotation = new File(PATH_TEMPLATES + "\\import_jsog_entity.txt");

					}

					lines.add(importLine + 1, FileUtils.readFileToString(fileImportAnnotation, "UTF-8"));
					lines.add(entityLine + 2, FileUtils.readFileToString(fileAnnotationEntity, "UTF-8"));
					Files.write(path, lines, StandardCharsets.UTF_8);
					System.out.println("=================================");
					System.out.println("importLine = " + importLine);
					System.out.println("entityLine = " + entityLine);
					System.out.println("=================================");

				}
				if (createRepositoryFiles) {
					String entityName = file.getName();
					entityName = entityName.substring(0, entityName.lastIndexOf("."));
					String idDataType = "Integer";
					Map<String, String> valuesMap = new HashMap<String, String>();
					valuesMap.put("packageName", packageName);
					valuesMap.put("entityName", entityName);
					valuesMap.put("idDataType", idDataType);
					StrSubstitutor sub = new StrSubstitutor(valuesMap);

					String templateRepositoryString = new String(Files.readAllBytes(
							Paths.get(PATH_TEMPLATES + "\\repository_template\\JpaRepositoryTemplate.java")));
					String templateEntityInterfaceCustom = new String(Files.readAllBytes(
							Paths.get(PATH_TEMPLATES + "\\repository_template\\EntityCustomInterface.java")));
					String templateEntityRepositoryImplemented = new String(Files.readAllBytes(
							Paths.get(PATH_TEMPLATES + "\\repository_template\\EntityRepositoryImpl.java")));
					;

					String resolvedString = sub.replace(templateRepositoryString);
					String interfaceResolvedString = sub.replace(templateEntityInterfaceCustom);
					String entityRepositoryImplementedString = sub.replace(templateEntityRepositoryImplemented);

					File entityPath = new File(repositoryPath.getAbsolutePath() + "\\" + entityName);
					if (entityPath.exists()) {
						for (File f : entityPath.listFiles()) {
							if (f.getName().endsWith("Repository.java"))
								writeAndMoveFileIfAlreadyExists(f, resolvedString);
							if (f.getName().endsWith("RepositoryImpl.java"))
								writeAndMoveFileIfAlreadyExists(f, entityRepositoryImplementedString);
							if (f.getName().endsWith("RepositoryCustomInterface.java"))
								writeAndMoveFileIfAlreadyExists(f, interfaceResolvedString);
						}
						entityPath.delete();
					}
					entityPath.mkdir();

					PrintWriter writer = new PrintWriter(repositoryPath.getAbsolutePath() + "\\" + entityName + "\\"
							+ entityName + "Repository.java", "UTF-8");
					writer.write(resolvedString);
					writer.close();
					writer = new PrintWriter(repositoryPath.getAbsolutePath() + "\\" + entityName + "\\" + entityName
							+ "RepositoryCustomInterface.java", "UTF-8");
					writer.write(interfaceResolvedString);
					writer.close();
					writer = new PrintWriter(repositoryPath.getAbsolutePath() + "\\" + entityName + "\\" + entityName
							+ "RepositoryImpl.java", "UTF-8");
					writer.write(entityRepositoryImplementedString);
					writer.close();
				}
				if (createRestFiles) {
					String entityName = file.getName();
					entityName = entityName.substring(0, entityName.lastIndexOf("."));
					String lowerCaseEntityName = entityName.toLowerCase();
					String idDataType = "Integer";
					Map<String, String> valuesMap = new HashMap<String, String>();
					valuesMap.put("packageName", packageName);
					valuesMap.put("entityName", entityName);
					valuesMap.put("lowerCaseEntityName", lowerCaseEntityName);
					valuesMap.put("idDataType", idDataType);
					StrSubstitutor sub = new StrSubstitutor(valuesMap);

					String templateEntityService = new String(
							Files.readAllBytes(Paths.get(PATH_TEMPLATES + "\\rest_template\\EntityService.java")));
					String templateEntityServiceImpl = new String(
							Files.readAllBytes(Paths.get(PATH_TEMPLATES + "\\rest_template\\EntityServiceImpl.java")));

					String entityServiceResolvedString = sub.replace(templateEntityService);
					String entityServiceImplResolvedString = sub.replace(templateEntityServiceImpl);

					File entityPath = new File(restPath.getAbsolutePath() + "\\" + entityName);
					System.out.println("=======_________=======_________=======_________");
					System.out.println(entityPath.getAbsolutePath());
					if (entityPath.exists()) {
						for (File f : entityPath.listFiles()) {
							if (f.getName().endsWith("Service.java"))
								writeAndMoveFileIfAlreadyExists(f, entityServiceResolvedString);

							if (f.getName().endsWith("ServiceImpl.java"))
								writeAndMoveFileIfAlreadyExists(f, entityServiceImplResolvedString);
						}
					}
					entityPath.mkdir();

					PrintWriter writer = new PrintWriter(
							restPath.getAbsolutePath() + "\\" + entityName + "\\" + entityName + "Service.java",
							"UTF-8");
					writer.write(entityServiceResolvedString);
					writer.close();
					writer = new PrintWriter(
							restPath.getAbsolutePath() + "\\" + entityName + "\\" + entityName + "ServiceImpl.java",
							"UTF-8");
					writer.write(entityServiceImplResolvedString);
					writer.close();
				}
			}
		} else {
			System.out.println("no es directorio");
		}
		System.out.println(domainPath.toString());
	}

	public String getPackageName() {
		String path = this.getClass().getName();
		return path.substring(0, path.lastIndexOf("."));
	}

	public String getFullPackagePath() {
		String internalPath = this.getClass().getName().replace(".", File.separator);
		String externalPath = System.getProperty("user.dir") + File.separator + "src\\main\\java";
		String workDir = externalPath + File.separator
				+ internalPath.substring(0, internalPath.lastIndexOf(File.separator));
		return workDir;
	}

	public static void writeAndMoveFileIfAlreadyExists(File file, String originalContent) throws IOException {

		if (originalContent.equals(FileUtils.readFileToString(file, "UTF-8"))) {
			file.delete();
		} else {
			File target = new File(file.getAbsolutePath() + ".bak");
			if (target.exists())
				target.delete();
			file.renameTo(target);
		}

	}

}
