package com.dennisbot.minsa.controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.Parent;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.hibernate.hql.internal.ast.tree.MapValueNode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainController {

	@RequestMapping("/index")
	public String home(Map<String, Object> model) {
		model.put("message", "HowToDoInJava Reader !!");
		return "index";
	}

	@RequestMapping("/next")
	public String next(Map<String, Object> model) {
		model.put("message", "You are in new page !!");
		return "next";
	}
	
	@RequestMapping(value = "/saludar/{word}", method = RequestMethod.GET)
	@ResponseBody
	public String saludo(@PathVariable("word") String word) {
		return "hola " + (word.isEmpty() ? " (nombre no indicado)" : word);
	}

	@RequestMapping(value = "/saludar", method = RequestMethod.GET)
	public String saludo() {
		return "hola (nombre no indicado)";
	}

	@RequestMapping("/info")
	@ResponseBody
	public String index() throws FileNotFoundException, IOException, XmlPullParserException {
		MavenXpp3Reader reader = new MavenXpp3Reader();
		Model model = reader.read(new FileReader("pom.xml"));
		String info = "model.getId() = " + model.getId() + "<br />";
		info += "model.getGroupId() = " + model.getGroupId() + "<br />";
		info += "model.getArtifactId() = " + model.getArtifactId() + "<br />";
		info += "model.getVersion() = " + model.getVersion() + "<br /><br /> Parent Info:<br />";
		Parent parentModel = model.getParent();
		info += "parentModel.getGroupId() = " + parentModel.getGroupId() + "<br />";
		info += "parentModel.getVersion() = " + parentModel.getVersion() + "<br />";

		return info + "Proudly handcrafted by " + "<a href='http://netgloo.com/en'>Netgloo</a>"
				+ " <br /> Dennisbot sin cambiar id = 3 de camucita" + "<br /> y se eliminó el id = 2";
	}
}
