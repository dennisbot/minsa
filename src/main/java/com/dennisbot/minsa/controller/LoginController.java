
package com.dennisbot.minsa.controller;
import java.util.List;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dennisbot.minsa.domain.PersonalSalud;
import com.dennisbot.minsa.repository.PersonalSalud.PersonalSaludRepository;
import com.dennisbot.minsa.utils.HibernateAwareObjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class LoginController {
	@Autowired
	private PersonalSaludRepository personalSaludRepository;
//	@Autowired
//	private CargoService cargoService;

	@RequestMapping("/")
	public String main(HttpSession session) {
		if (session.getAttribute("usuario") == null)
			return "redirect:/login";

//		System.out.println("entró al controlador main root /");
		return "main";
	}

	@RequestMapping("/login")
	public String login(Model model, HttpSession session) {
		if (session.getAttribute("usuario") != null)
			return "redirect:/";
		System.out.println("llega :)");
		model.addAttribute("loginUser", new PersonalSalud());
		return "login";
	}
	@RequestMapping(value = "/getline", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<PersonalSalud> getline() 
			throws Exception {
		System.out.println("entra a getline");
		List<PersonalSalud> p = personalSaludRepository.getFirstNamesLike("dsaf");
		HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
		String var = mapper.writeValueAsString(p);
		System.out.println("var personal as string:");
		System.out.println(var);
		return p;
//		return "redirect:/login";
	}
	@RequestMapping(value = "/login", method=RequestMethod.POST)
	public String dologin(@ModelAttribute("loginUser") PersonalSalud personal, HttpSession session, Model model) 
			throws Exception {
		
		HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
		String var = mapper.writeValueAsString(personal);
		System.out.println("var personal as string antes:");
		System.out.println(var);
		boolean canLogin = personalSaludRepository.canLogIn(personal);
		
		var = mapper.writeValueAsString(personal);
		System.out.println("var personal as string despues:");
		System.out.println(var);
		System.out.println("============================================");
		System.out.println(personal.getNombres());
		System.out.println(personal.getPaterno());
		System.out.println(personal.getMaterno());
		System.out.println(personal.getUsuario());
		System.out.println("============================================");
		if (canLogin) {
			session.setAttribute("usuario", personal);
			return "redirect:/";
		}
		model.addAttribute("error", "usuario y/o contraseña incorrectos");
		//se puede usar ambos, pero mejor es con redirect (pero se pierde los atributos de model)
		return "login";
//		return "redirect:/login";
	}

	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		// finalizar la sesión y luego redirigir a login
		session.invalidate();
		return "redirect:/login";
	}
}
