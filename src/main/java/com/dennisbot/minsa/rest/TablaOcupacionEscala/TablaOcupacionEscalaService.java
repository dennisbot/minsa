package com.dennisbot.minsa.rest.TablaOcupacionEscala;

import com.dennisbot.minsa.domain.TablaOcupacionEscala;

public interface TablaOcupacionEscalaService {
	public Iterable<TablaOcupacionEscala> findAll();
	public TablaOcupacionEscala findOne(Integer idTablaOcupacionEscala);
	public TablaOcupacionEscala create(TablaOcupacionEscala TablaOcupacionEscala);
	public TablaOcupacionEscala update(TablaOcupacionEscala TablaOcupacionEscala);
	public boolean delete(Integer idTablaOcupacionEscala);
}
