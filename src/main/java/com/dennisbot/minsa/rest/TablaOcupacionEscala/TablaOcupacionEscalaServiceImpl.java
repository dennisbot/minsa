package com.dennisbot.minsa.rest.TablaOcupacionEscala;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dennisbot.minsa.domain.TablaOcupacionEscala;
import com.dennisbot.minsa.repository.Parametro.ParametroRepository;
import com.dennisbot.minsa.repository.ParametroDetalle.ParametroDetalleRepository;
import com.dennisbot.minsa.repository.TablaOcupacionEscala.TablaOcupacionEscalaRepository;
import com.dennisbot.minsa.utils.HibernateAwareObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("/rest/tablaocupacionescala")
public class TablaOcupacionEscalaServiceImpl implements TablaOcupacionEscalaService {
    @Autowired
    private TablaOcupacionEscalaRepository tablaocupacionescalaRepository;
    @Autowired
	private ParametroDetalleRepository parametroDetalleRepository;
    @Autowired
	private ParametroRepository parametroRepository;
    
    @RequestMapping(
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public Iterable<TablaOcupacionEscala> findAll() {
    	Iterable<TablaOcupacionEscala> ll = tablaocupacionescalaRepository.findAll();
//    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
//		try {
//			String val;
//			val = mapper.writeValueAsString(ll);
//			System.out.println("list of tablaocupacionescala:");
//	    	System.out.println(val);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	return ll;
    }
    @RequestMapping(value="{id}",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public TablaOcupacionEscala findOne(@PathVariable("id") Integer idTablaOcupacionEscala) {
    	return tablaocupacionescalaRepository.findOne(idTablaOcupacionEscala);
    }
    @RequestMapping(
		method = RequestMethod.POST,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public TablaOcupacionEscala create(@RequestBody TablaOcupacionEscala tablaocupacionescala) {
		return tablaocupacionescalaRepository.save(tablaocupacionescala);
	}
    @RequestMapping(value="{id}",
		method = RequestMethod.PUT,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public TablaOcupacionEscala update(@RequestBody TablaOcupacionEscala tablaocupacionescala) {

    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
    	String val;
		try {
			 val = mapper.writeValueAsString(tablaocupacionescala);
//			 System.out.println("tablaocupacionescala:");
//	     	System.out.println(val);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tablaocupacionescalaRepository.save(tablaocupacionescala);
	}
    @RequestMapping(value="{id}",
    		method = RequestMethod.DELETE,
    		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public boolean delete(@PathVariable("id") Integer idTablaOcupacionEscala) {
	try {
			tablaocupacionescalaRepository.delete(idTablaOcupacionEscala);
			return true;
		} catch (Exception e) {
			System.out.println("mensaje de error:" + e.getMessage());
			return false;
		}
	}
    @RequestMapping(value = "escalasRemunerativasViewParams", method = RequestMethod.GET, produces = "application/json")
	public Map<String, Object> escalaRemunerativasViewParams() {
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		mapa.put("periodos", parametroDetalleRepository
				.findByIdParametro(parametroRepository.findTopByDescripcionTabla("PERIODO_ESCALA").getIdParametro()));
		mapa.put("escalas", parametroDetalleRepository
				.findByIdParametro(parametroRepository.findTopByDescripcionTabla("ESCALA").getIdParametro()));
		return mapa;
	}
    @RequestMapping(
    		value = "by/idperiodoescala/{id_periodo_escala}/idescala/{id_escala}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE
    	)
    public Iterable<TablaOcupacionEscala> findByPeriodoEscalaAndEscala(
    		@PathVariable("id_periodo_escala") Integer idPeriodoEscala,
    		@PathVariable("id_escala") Integer idEscala
	) {
    	Integer idParametro = 
    			parametroRepository.findTopByDescripcionTabla("OCUPACION_CARGO_PERSONAL")
    			.getIdParametro();
    	
    	System.out.println("idParametro : " + idParametro);
    	System.out.println("idPeriodoEscala : " + idPeriodoEscala);
    	System.out.println("idEscala : " + idEscala);
    	Iterable<TablaOcupacionEscala> ll = tablaocupacionescalaRepository
    			.getTable(idParametro, idPeriodoEscala, idEscala);
    	return ll;
    }
}


