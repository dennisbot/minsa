package com.dennisbot.minsa.rest.ParametroDetalle;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dennisbot.minsa.domain.ParametroDetalle;
import com.dennisbot.minsa.domain.custom.UpdateOrder;
import com.dennisbot.minsa.domain.projection.PDetalle;
import com.dennisbot.minsa.repository.ParametroDetalle.ParametroDetalleRepository;
import com.dennisbot.minsa.utils.HibernateAwareObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("/rest/parametrodetalle")
public class ParametroDetalleServiceImpl implements ParametroDetalleService {
    @Autowired
    private ParametroDetalleRepository parametrodetalleRepository;

    @RequestMapping(
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public Iterable<ParametroDetalle> findAll() {
    	Iterable<ParametroDetalle> ll = parametrodetalleRepository.findAll();
//    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
//		try {
//			String val;
//			val = mapper.writeValueAsString(ll);
//			System.out.println("list of parametrodetalle:");
//	    	System.out.println(val);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	return ll;
    }
    @RequestMapping(value="parametro/{idParametro}",
    		method = RequestMethod.GET,
    		produces = MediaType.APPLICATION_JSON_VALUE
    	)
        public Iterable<ParametroDetalle> findbyidparametro(@PathVariable("idParametro") Integer idParametro) {
        	return parametrodetalleRepository.findByIdParametroFull(idParametro);
        }
    @RequestMapping(value="{id}",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public ParametroDetalle findOne(@PathVariable("id") Integer idParametroDetalle) {
    	return parametrodetalleRepository.findOne(idParametroDetalle);
    }
    @RequestMapping(
		method = RequestMethod.POST,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public ParametroDetalle create(@RequestBody ParametroDetalle parametrodetalle) {
		return parametrodetalleRepository.save(parametrodetalle);
	}
    @RequestMapping(
    		value="bulkupdate",
    		method = RequestMethod.POST,
    		consumes = MediaType.APPLICATION_JSON_VALUE,
    		produces = MediaType.APPLICATION_JSON_VALUE
    	)
    	public boolean bulkUpdate(@RequestBody List<UpdateOrder> items) {
    	try {
    		return parametrodetalleRepository.bulkUpdatePosition(items);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
    }
    @RequestMapping(value="{id}",
		method = RequestMethod.PUT,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public ParametroDetalle update(@RequestBody ParametroDetalle parametrodetalle) {

    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
    	String val;
		try {
			 val = mapper.writeValueAsString(parametrodetalle);
//			 System.out.println("parametrodetalle:");
//	     	System.out.println(val);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return parametrodetalleRepository.save(parametrodetalle);
	}
    @RequestMapping(value="{id}",
    		method = RequestMethod.DELETE,
    		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public boolean delete(@PathVariable("id") Integer idParametroDetalle) {
	try {
			parametrodetalleRepository.delete(idParametroDetalle);
			return true;
		} catch (Exception e) {
			System.out.println("mensaje de error:" + e.getMessage());
			return false;
		}
	}
}


