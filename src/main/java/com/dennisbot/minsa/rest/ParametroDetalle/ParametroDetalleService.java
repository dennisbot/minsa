package com.dennisbot.minsa.rest.ParametroDetalle;

import com.dennisbot.minsa.domain.ParametroDetalle;

public interface ParametroDetalleService {
	public Iterable<ParametroDetalle> findAll();
	public ParametroDetalle findOne(Integer idParametroDetalle);
	public ParametroDetalle create(ParametroDetalle ParametroDetalle);
	public ParametroDetalle update(ParametroDetalle ParametroDetalle);
	public boolean delete(Integer idParametroDetalle);
}
