package com.dennisbot.minsa.rest.Asistencia;

import com.dennisbot.minsa.domain.Asistencia;

public interface AsistenciaService {
	public Iterable<Asistencia> findAll();
	public Asistencia findOne(Integer idAsistencia);
	public Asistencia create(Asistencia Asistencia);
	public Asistencia update(Asistencia Asistencia);
	public boolean delete(Integer idAsistencia);
}
