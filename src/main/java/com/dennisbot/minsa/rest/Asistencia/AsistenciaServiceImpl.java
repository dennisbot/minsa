package com.dennisbot.minsa.rest.Asistencia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dennisbot.minsa.domain.Asistencia;
import com.dennisbot.minsa.repository.Asistencia.AsistenciaRepository;
import com.dennisbot.minsa.utils.HibernateAwareObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("/rest/asistencia")
public class AsistenciaServiceImpl implements AsistenciaService {
    @Autowired
    private AsistenciaRepository asistenciaRepository;

    @RequestMapping(
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public Iterable<Asistencia> findAll() {
    	Iterable<Asistencia> ll = asistenciaRepository.findAll();
//    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
//		try {
//			String val;
//			val = mapper.writeValueAsString(ll);
//			System.out.println("list of asistencia:");
//	    	System.out.println(val);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	return ll;
    }
    @RequestMapping(value="{id}",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public Asistencia findOne(@PathVariable("id") Integer idAsistencia) {
    	return asistenciaRepository.findOne(idAsistencia);
    }
    @RequestMapping(
		method = RequestMethod.POST,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public Asistencia create(@RequestBody Asistencia asistencia) {
		return asistenciaRepository.save(asistencia);
	}
    @RequestMapping(value="{id}",
		method = RequestMethod.PUT,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public Asistencia update(@RequestBody Asistencia asistencia) {

    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
    	String val;
		try {
			 val = mapper.writeValueAsString(asistencia);
//			 System.out.println("asistencia:");
//	     	System.out.println(val);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return asistenciaRepository.save(asistencia);
	}
    @RequestMapping(value="{id}",
    		method = RequestMethod.DELETE,
    		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public boolean delete(@PathVariable("id") Integer idAsistencia) {
	try {
			asistenciaRepository.delete(idAsistencia);
			return true;
		} catch (Exception e) {
			System.out.println("mensaje de error:" + e.getMessage());
			return false;
		}
	}
}


