package com.dennisbot.minsa.rest.Vacaciones;

import com.dennisbot.minsa.domain.Vacaciones;

public interface VacacionesService {
	public Iterable<Vacaciones> findAll();
	public Vacaciones findOne(Integer idVacaciones);
	public Vacaciones create(Vacaciones Vacaciones);
	public Vacaciones update(Vacaciones Vacaciones);
	public boolean delete(Integer idVacaciones);
}
