package com.dennisbot.minsa.rest.LicenciaPermiso;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dennisbot.minsa.domain.LicenciaPermiso;
import com.dennisbot.minsa.repository.LicenciaPermiso.LicenciaPermisoRepository;
import com.dennisbot.minsa.utils.HibernateAwareObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("/rest/licenciapermiso")
public class LicenciaPermisoServiceImpl implements LicenciaPermisoService {
    @Autowired
    private LicenciaPermisoRepository licenciapermisoRepository;

    @RequestMapping(
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public Iterable<LicenciaPermiso> findAll() {
    	Iterable<LicenciaPermiso> ll = licenciapermisoRepository.findAll();
//    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
//		try {
//			String val;
//			val = mapper.writeValueAsString(ll);
//			System.out.println("list of licenciapermiso:");
//	    	System.out.println(val);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	return ll;
    }
    @RequestMapping(value="{id}",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public LicenciaPermiso findOne(@PathVariable("id") Integer idLicenciaPermiso) {
    	return licenciapermisoRepository.findOne(idLicenciaPermiso);
    }
    @RequestMapping(
		method = RequestMethod.POST,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public LicenciaPermiso create(@RequestBody LicenciaPermiso licenciapermiso) {
		return licenciapermisoRepository.save(licenciapermiso);
	}
    @RequestMapping(value="{id}",
		method = RequestMethod.PUT,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public LicenciaPermiso update(@RequestBody LicenciaPermiso licenciapermiso) {

    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
    	String val;
		try {
			 val = mapper.writeValueAsString(licenciapermiso);
//			 System.out.println("licenciapermiso:");
//	     	System.out.println(val);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return licenciapermisoRepository.save(licenciapermiso);
	}
    @RequestMapping(value="{id}",
    		method = RequestMethod.DELETE,
    		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public boolean delete(@PathVariable("id") Integer idLicenciaPermiso) {
	try {
			licenciapermisoRepository.delete(idLicenciaPermiso);
			return true;
		} catch (Exception e) {
			System.out.println("mensaje de error:" + e.getMessage());
			return false;
		}
	}
}


