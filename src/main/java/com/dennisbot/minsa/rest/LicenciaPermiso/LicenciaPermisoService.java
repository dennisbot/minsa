package com.dennisbot.minsa.rest.LicenciaPermiso;

import com.dennisbot.minsa.domain.LicenciaPermiso;

public interface LicenciaPermisoService {
	public Iterable<LicenciaPermiso> findAll();
	public LicenciaPermiso findOne(Integer idLicenciaPermiso);
	public LicenciaPermiso create(LicenciaPermiso LicenciaPermiso);
	public LicenciaPermiso update(LicenciaPermiso LicenciaPermiso);
	public boolean delete(Integer idLicenciaPermiso);
}
