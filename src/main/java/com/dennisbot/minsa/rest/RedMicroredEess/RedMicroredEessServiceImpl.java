package com.dennisbot.minsa.rest.RedMicroredEess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dennisbot.minsa.domain.RedMicroredEess;
import com.dennisbot.minsa.repository.RedMicroredEess.RedMicroredEessRepository;
import com.dennisbot.minsa.utils.HibernateAwareObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("/rest/redmicroredeess")
public class RedMicroredEessServiceImpl implements RedMicroredEessService {
    @Autowired
    private RedMicroredEessRepository redmicroredeessRepository;

    @RequestMapping(
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public Iterable<RedMicroredEess> findAll() {
    	Iterable<RedMicroredEess> ll = redmicroredeessRepository.findAll();
//    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
//		try {
//			String val;
//			val = mapper.writeValueAsString(ll);
//			System.out.println("list of redmicroredeess:");
//	    	System.out.println(val);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	return ll;
    }
    @RequestMapping(value="{id}",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public RedMicroredEess findOne(@PathVariable("id") Integer idRedMicroredEess) {
    	return redmicroredeessRepository.findOne(idRedMicroredEess);
    }
    @RequestMapping(
		method = RequestMethod.POST,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public RedMicroredEess create(@RequestBody RedMicroredEess redmicroredeess) {
		return redmicroredeessRepository.save(redmicroredeess);
	}
    @RequestMapping(value="{id}",
		method = RequestMethod.PUT,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public RedMicroredEess update(@RequestBody RedMicroredEess redmicroredeess) {

    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
    	String val;
		try {
			 val = mapper.writeValueAsString(redmicroredeess);
//			 System.out.println("redmicroredeess:");
//	     	System.out.println(val);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return redmicroredeessRepository.save(redmicroredeess);
	}
    @RequestMapping(value="{id}",
    		method = RequestMethod.DELETE,
    		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public boolean delete(@PathVariable("id") Integer idRedMicroredEess) {
	try {
			redmicroredeessRepository.delete(idRedMicroredEess);
			return true;
		} catch (Exception e) {
			System.out.println("mensaje de error:" + e.getMessage());
			return false;
		}
	}
}


