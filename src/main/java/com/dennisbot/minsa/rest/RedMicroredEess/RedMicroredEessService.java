package com.dennisbot.minsa.rest.RedMicroredEess;

import com.dennisbot.minsa.domain.RedMicroredEess;

public interface RedMicroredEessService {
	public Iterable<RedMicroredEess> findAll();
	public RedMicroredEess findOne(Integer idRedMicroredEess);
	public RedMicroredEess create(RedMicroredEess RedMicroredEess);
	public RedMicroredEess update(RedMicroredEess RedMicroredEess);
	public boolean delete(Integer idRedMicroredEess);
}
