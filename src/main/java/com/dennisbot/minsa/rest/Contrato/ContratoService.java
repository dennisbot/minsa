package com.dennisbot.minsa.rest.Contrato;

import com.dennisbot.minsa.domain.Contrato;

public interface ContratoService {
	public Iterable<Contrato> findAll();
	public Contrato findOne(Integer idContrato);
	public Contrato create(Contrato Contrato);
	public Contrato update(Contrato Contrato);
	public boolean delete(Integer idContrato);
}
