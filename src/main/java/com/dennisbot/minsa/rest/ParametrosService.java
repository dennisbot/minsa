package com.dennisbot.minsa.rest;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dennisbot.minsa.domain.Parametro;
import com.dennisbot.minsa.domain.ParametroDetalle;
import com.dennisbot.minsa.domain.PersonalSalud;
import com.dennisbot.minsa.domain.projection.PDetalle;
import com.dennisbot.minsa.repository.Parametro.ParametroRepository;
import com.dennisbot.minsa.repository.ParametroDetalle.ParametroDetalleRepository;
import com.dennisbot.minsa.repository.PersonalSalud.PersonalSaludRepository;

@RestController
@RequestMapping("/rest/parametros")
public class ParametrosService {
	@Autowired
	private PersonalSaludRepository personalSaludRepository;
	@Autowired
	private ParametroDetalleRepository parametroDetalleRepository;
	@Autowired
	private ParametroRepository parametroRepository;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public Map<String, Object> parametros(HttpSession session) {
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		PersonalSalud personalSalud = (PersonalSalud) session.getAttribute("usuario");
		mapa.put("usuario", personalSalud);
		return mapa;
	}

	@RequestMapping(value = "personalSaludViewParams", method = RequestMethod.GET, produces = "application/json")
	public Map<String, Object> personalSaludViewParams() {
		HashMap<String, Object> mapa = new HashMap<String, Object>();
		mapa.put("personalesSalud", personalSaludRepository.findAllWithOcupacion());
		mapa.put("profesiones", parametroDetalleRepository
				.findByIdParametro(parametroRepository.findTopByDescripcionTabla("PROFESION").getIdParametro()));
		mapa.put("tiposDocumentos", parametroDetalleRepository
				.findByIdParametro(parametroRepository.findTopByDescripcionTabla("TIPO_DOCUMENTO").getIdParametro()));

		return mapa;
	}

	@RequestMapping(value = "parametro", method = RequestMethod.GET, produces = "application/json")
	public Iterable<PDetalle> parametro() {
		return parametroDetalleRepository.findByIdParametro(7);
		// parametroRepository.findTopByDescripcionTabla("TIPO_DOCUMENTO").getIdParametro()
	}

	@RequestMapping(value = "profesiones", method = RequestMethod.GET, produces = "application/json")
	public Iterable<PDetalle> profesiones() {
		return parametroDetalleRepository
				.findByIdParametro(parametroRepository.findTopByDescripcionTabla("PROFESION").getIdParametro());
	}

	@RequestMapping(value = "profesion", method = RequestMethod.GET, produces = "application/json")
	public Parametro profesion() {
		return parametroRepository.findTopByDescripcionTabla("PROFESION");
	}

	@RequestMapping(value = "idparametro", method = RequestMethod.GET, produces = "application/json")
	public Iterable<Parametro> idparametro() {
		return parametroRepository.findIdParametroByDescripcionTabla("TIPO_DOCUMENTO");
	}

	@RequestMapping(value = "top", method = RequestMethod.GET, produces = "application/json")
	public Parametro top() {
		return parametroRepository.findTopByDescripcionTabla("TIPO_DOCUMENTO");
	}

	// @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces =
	// "application/json")
	// public @ResponseBody Member lookupMemberById(@PathVariable("id") Long id)
	// {
	// return memberDao.findById(id);
	// }
}
