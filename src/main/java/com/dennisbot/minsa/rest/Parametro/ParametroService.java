package com.dennisbot.minsa.rest.Parametro;

import com.dennisbot.minsa.domain.Parametro;

public interface ParametroService {
	public Iterable<Parametro> findAll();
	public Parametro findOne(Integer idParametro);
	public Parametro create(Parametro Parametro);
	public Parametro update(Parametro Parametro);
	public boolean delete(Integer idParametro);
}
