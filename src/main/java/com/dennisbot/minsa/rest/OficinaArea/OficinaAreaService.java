package com.dennisbot.minsa.rest.OficinaArea;

import com.dennisbot.minsa.domain.OficinaArea;

public interface OficinaAreaService {
	public Iterable<OficinaArea> findAll();
	public OficinaArea findOne(Integer idOficinaArea);
	public OficinaArea create(OficinaArea OficinaArea);
	public OficinaArea update(OficinaArea OficinaArea);
	public boolean delete(Integer idOficinaArea);
}
