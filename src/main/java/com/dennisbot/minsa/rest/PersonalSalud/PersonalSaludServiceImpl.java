package com.dennisbot.minsa.rest.PersonalSalud;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dennisbot.minsa.domain.PersonalSalud;
import com.dennisbot.minsa.domain.projection.PDetalle;
import com.dennisbot.minsa.repository.PersonalSalud.PersonalSaludRepository;
import com.dennisbot.minsa.utils.HibernateAwareObjectMapper;
import com.dennisbot.minsa.utils.WebUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/rest/personalsalud")
public class PersonalSaludServiceImpl implements PersonalSaludService {
    @Autowired
    private PersonalSaludRepository personalsaludRepository;

    @RequestMapping(
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public Iterable<PersonalSalud> findAll() {
//    	Iterable<PersonalSalud> ll = personalsaludRepository.findAll();
    	Iterable<PersonalSalud> ll = personalsaludRepository.findAllWithOcupacion();
//    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
//		try {
//			String val;
//			val = mapper.writeValueAsString(ll);
//			System.out.println("list of personalsalud:");
//	    	System.out.println(val);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	return ll;
    }
    @RequestMapping(value="{id}",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public PersonalSalud findOne(@PathVariable("id") Integer idPersonalSalud) {
    	return personalsaludRepository.findFirstByIdPersonalSalud(idPersonalSalud);
    }
    @RequestMapping(
		method = RequestMethod.POST,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public PersonalSalud create(@RequestBody PersonalSalud personalsalud) throws JsonProcessingException {
    	WebUtil.check_debug(personalsalud);
		return personalsaludRepository.save(personalsalud);
	}
    @RequestMapping(value="{id}",
		method = RequestMethod.PUT,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public PersonalSalud update(@RequestBody PersonalSalud personalsalud) {

    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
    	String val;
		try {
			 val = mapper.writeValueAsString(personalsalud);
//			 System.out.println("personalsalud:");
//	     	System.out.println(val);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return personalsaludRepository.save(personalsalud);
	}
    @RequestMapping(value="{id}",
    		method = RequestMethod.DELETE,
    		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public boolean delete(@PathVariable("id") Integer idPersonalSalud) {
	try {
			personalsaludRepository.delete(idPersonalSalud);
			return true;
		} catch (Exception e) {
			System.out.println("mensaje de error:" + e.getMessage());
			return false;
		}
	}
    @RequestMapping(value = "updateProfile/{id}", 
    		method = RequestMethod.PUT, 
    		consumes = MediaType.APPLICATION_JSON_VALUE, 
    		produces = MediaType.APPLICATION_JSON_VALUE)
	public PersonalSalud updateProfile(@RequestBody PersonalSalud personalSalud,
			HttpSession session) {
		session.setAttribute("usuario", personalSalud);
		return personalsaludRepository.save(personalSalud);
	}

}
