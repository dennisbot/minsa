package com.dennisbot.minsa.rest.PersonalSalud;

import com.dennisbot.minsa.domain.PersonalSalud;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface PersonalSaludService {
	public Iterable<PersonalSalud> findAll();
	public PersonalSalud findOne(Integer idPersonalSalud);
	public PersonalSalud create(PersonalSalud PersonalSalud) throws JsonProcessingException;
	public PersonalSalud update(PersonalSalud PersonalSalud);
	public boolean delete(Integer idPersonalSalud);
}
