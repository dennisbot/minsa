package com.dennisbot.minsa.rest.UbicacionRegion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dennisbot.minsa.domain.UbicacionRegion;
import com.dennisbot.minsa.repository.UbicacionRegion.UbicacionRegionRepository;
import com.dennisbot.minsa.utils.HibernateAwareObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("/rest/ubicacionregion")
public class UbicacionRegionServiceImpl implements UbicacionRegionService {
    @Autowired
    private UbicacionRegionRepository ubicacionregionRepository;

    @RequestMapping(
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public Iterable<UbicacionRegion> findAll() {
    	Iterable<UbicacionRegion> ll = ubicacionregionRepository.findAll();
//    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
//		try {
//			String val;
//			val = mapper.writeValueAsString(ll);
//			System.out.println("list of ubicacionregion:");
//	    	System.out.println(val);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	return ll;
    }
    @RequestMapping(value="{id}",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public UbicacionRegion findOne(@PathVariable("id") Integer idUbicacionRegion) {
    	return ubicacionregionRepository.findOne(idUbicacionRegion);
    }
    @RequestMapping(
		method = RequestMethod.POST,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public UbicacionRegion create(@RequestBody UbicacionRegion ubicacionregion) {
		return ubicacionregionRepository.save(ubicacionregion);
	}
    @RequestMapping(value="{id}",
		method = RequestMethod.PUT,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public UbicacionRegion update(@RequestBody UbicacionRegion ubicacionregion) {

    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
    	String val;
		try {
			 val = mapper.writeValueAsString(ubicacionregion);
//			 System.out.println("ubicacionregion:");
//	     	System.out.println(val);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ubicacionregionRepository.save(ubicacionregion);
	}
    @RequestMapping(value="{id}",
    		method = RequestMethod.DELETE,
    		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public boolean delete(@PathVariable("id") Integer idUbicacionRegion) {
	try {
			ubicacionregionRepository.delete(idUbicacionRegion);
			return true;
		} catch (Exception e) {
			System.out.println("mensaje de error:" + e.getMessage());
			return false;
		}
	}
}


