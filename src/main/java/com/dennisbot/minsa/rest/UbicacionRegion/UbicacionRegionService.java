package com.dennisbot.minsa.rest.UbicacionRegion;

import com.dennisbot.minsa.domain.UbicacionRegion;

public interface UbicacionRegionService {
	public Iterable<UbicacionRegion> findAll();
	public UbicacionRegion findOne(Integer idUbicacionRegion);
	public UbicacionRegion create(UbicacionRegion UbicacionRegion);
	public UbicacionRegion update(UbicacionRegion UbicacionRegion);
	public boolean delete(Integer idUbicacionRegion);
}
