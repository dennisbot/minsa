package com.dennisbot.minsa.rest.RolTurno;

import com.dennisbot.minsa.domain.RolTurno;

public interface RolTurnoService {
	public Iterable<RolTurno> findAll();
	public RolTurno findOne(Integer idRolTurno);
	public RolTurno create(RolTurno RolTurno);
	public RolTurno update(RolTurno RolTurno);
	public boolean delete(Integer idRolTurno);
}
