package com.dennisbot.minsa.rest.RolTurno;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dennisbot.minsa.domain.RolTurno;
import com.dennisbot.minsa.repository.RolTurno.RolTurnoRepository;
import com.dennisbot.minsa.utils.HibernateAwareObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("/rest/rolturno")
public class RolTurnoServiceImpl implements RolTurnoService {
    @Autowired
    private RolTurnoRepository rolturnoRepository;

    @RequestMapping(
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public Iterable<RolTurno> findAll() {
    	Iterable<RolTurno> ll = rolturnoRepository.findAll();
//    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
//		try {
//			String val;
//			val = mapper.writeValueAsString(ll);
//			System.out.println("list of rolturno:");
//	    	System.out.println(val);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    	return ll;
    }
    @RequestMapping(value="{id}",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
    public RolTurno findOne(@PathVariable("id") Integer idRolTurno) {
    	return rolturnoRepository.findOne(idRolTurno);
    }
    @RequestMapping(
		method = RequestMethod.POST,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public RolTurno create(@RequestBody RolTurno rolturno) {
		return rolturnoRepository.save(rolturno);
	}
    @RequestMapping(value="{id}",
		method = RequestMethod.PUT,
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public RolTurno update(@RequestBody RolTurno rolturno) {

    	HibernateAwareObjectMapper mapper = new HibernateAwareObjectMapper();
    	String val;
		try {
			 val = mapper.writeValueAsString(rolturno);
//			 System.out.println("rolturno:");
//	     	System.out.println(val);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rolturnoRepository.save(rolturno);
	}
    @RequestMapping(value="{id}",
    		method = RequestMethod.DELETE,
    		produces = MediaType.APPLICATION_JSON_VALUE
	)
	public boolean delete(@PathVariable("id") Integer idRolTurno) {
	try {
			rolturnoRepository.delete(idRolTurno);
			return true;
		} catch (Exception e) {
			System.out.println("mensaje de error:" + e.getMessage());
			return false;
		}
	}
}


