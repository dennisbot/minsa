package com.dennisbot.minsa.domain.projection;

import java.io.Serializable;

public class PDetalle implements Serializable {
   public int key;
   public String value;

   public PDetalle(int key, String value) {
       this.key = key;
       this.value = value;
   }
}