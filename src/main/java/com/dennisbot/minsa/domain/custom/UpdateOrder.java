package com.dennisbot.minsa.domain.custom;

import java.io.Serializable;

public class UpdateOrder implements Serializable {
	public int idParametroDetalle;
	public int orden;
	
	public UpdateOrder() {
	}

	public UpdateOrder(int idParametroDetalle, int orden) {
		this.idParametroDetalle = idParametroDetalle;
		this.orden = orden;
	}

	public int getIdParametroDetalle() {
		return idParametroDetalle;
	}

	public void setIdParametroDetalle(int idParametroDetalle) {
		this.idParametroDetalle = idParametroDetalle;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

}